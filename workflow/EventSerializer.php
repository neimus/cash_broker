<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow;

use app\helpers\ClassHelper;
use yii\queue\serializers\JsonSerializer;

class EventSerializer extends JsonSerializer
{
    const QUEUE_EVENT_NAMESPACE = 'app\workflow\event';

    /**
     * @var string
     */
    public static $commandKey = 'command';

    /**
     * @var string
     */
    public static $suffixClass = 'QueueEvent';

    /**
     * @param $data
     *
     * @return mixed
     */
    protected function fromArray($data)
    {
        if (\is_array($data) && isset($data[self::$commandKey])) {
            $class = ClassHelper::getNamespaceByShortName(self::QUEUE_EVENT_NAMESPACE, $data[self::$commandKey],
                self::$suffixClass);
            if (class_exists($class)) {
                $data[$this->classKey] = $class;
                unset($data[self::$commandKey]);
            }
        }

        return parent::fromArray($data);
    }
}
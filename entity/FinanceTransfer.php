<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\entity;

use app\models\Payout;
use app\models\Refill;
use app\models\Transaction;
use app\models\User;

class FinanceTransfer
{
    /**
     * @var User
     */
    private $fromUser;
    /**
     * @var User
     */
    private $toUser;

    /**
     * @var User
     */
    private $initiator;

    /**
     * @var string
     */
    private $transactionType;

    /**
     * @var int
     */
    private $fromAccount;

    /**
     * @var int
     */
    private $toAccount;

    /**
     * @var Refill|null
     */
    private $refill;

    /**
     * @var Payout|null
     */
    private $payout;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @var string
     */
    private $comment = '';

    /**
     * @return User
     */
    public function getFromUser(): User
    {
        return $this->fromUser;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setFromUser(User $user): self
    {
        $this->fromUser = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getToUser(): User
    {
        return $this->toUser;
    }

    /**
     * @param User $toUser
     *
     * @return self
     */
    public function setToUser(User $toUser): self
    {
        $this->toUser = $toUser;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getInitiator(): ?User
    {
        return $this->initiator;
    }

    /**
     * @param User $initiator
     *
     * @return self
     */
    public function setInitiator(User $initiator): self
    {
        $this->initiator = $initiator;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     *
     * @return self
     */
    public function setTransactionType(string $transactionType): self
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * @return int
     */
    public function getFromAccount(): int
    {
        return $this->fromAccount;
    }

    /**
     * @param int $fromAccount
     *
     * @return self
     */
    public function setFromAccount(int $fromAccount): self
    {
        $this->fromAccount = $fromAccount;

        return $this;
    }

    /**
     * @return int
     */
    public function getToAccount(): int
    {
        return $this->toAccount;
    }

    /**
     * @param int $toAccount
     *
     * @return self
     */
    public function setToAccount(int $toAccount): self
    {
        $this->toAccount = $toAccount;

        return $this;
    }

    /**
     * @return Refill|null
     */
    public function getRefill(): ?Refill
    {
        return $this->refill;
    }

    /**
     * @param Refill|null $refill
     *
     * @return self
     */
    public function setRefill(?Refill $refill): self
    {
        $this->refill = $refill;

        return $this;
    }

    /**
     * @return Payout|null
     */
    public function getPayout(): ?Payout
    {
        return $this->payout;
    }

    /**
     * @param Payout|null $payout
     *
     * @return self
     */
    public function setPayout(?Payout $payout): self
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     *
     * @return self
     */
    public function setAmount(Money $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return self
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function isValid(): bool
    {
        return ($this->hasRefill() || $this->hasPayout())
               && $this->hasUsers() && $this->hasAccounts()
               && $this->getAmount() !== null
               && \in_array($this->getTransactionType(), Transaction::TRANSACTION_TYPE, true);
    }

    public function hasRefill(): bool
    {
        return $this->getRefill() !== null;
    }

    public function hasPayout(): bool
    {
        return $this->getPayout() !== null;
    }

    public function hasInitiator(): bool
    {
        return $this->getInitiator() !== null;
    }

    public function hasUsers(): bool
    {
        return $this->getFromUser() !== null && $this->getToUser() !== null;
    }

    public function hasAccounts(): bool
    {
        return $this->getFromAccount() !== null && $this->getToAccount() !== null;
    }

    public function __toString()
    {
        return json_encode([
            'refill'           => $this->getRefill(),
            'payout'           => $this->getPayout(),
            'fromUser'         => $this->getFromUser(),
            'toUser'           => $this->getToUser(),
            'from_account'     => $this->getFromAccount(),
            'to_account'       => $this->getToAccount(),
            'amount'           => $this->getAmount(),
            'transaction_type' => $this->getTransactionType(),
            'initiator'        => $this->getInitiator(),
            'comment'          => $this->getComment(),
        ]);
    }

}
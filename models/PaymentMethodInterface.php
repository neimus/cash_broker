<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\models;

interface PaymentMethodInterface
{
    const METHOD_OTHER             = 'other';
    const METHOD_CREDIT_CARD       = 'credit_card';
    const METHOD_YANDEX_MONEY      = 'yandex_money';
    const METHOD_SBERBANK_ONLINE   = 'sberbank_online';
    const METHOD_MOBILE_MTS        = 'mobile_mts';
    const METHOD_MOBILE_BEELINE    = 'mobile_beeline';
    const METHOD_MOBILE_TELE2      = 'mobile_tele2';
    const METHOD_MOBILE_MEGAFON    = 'mobile_megafon';
    const METHOD_QIWI_WALLET       = 'qiwi_wallet';
    const METHOD_MONETA_RU         = 'moneta_ru';
    const METHOD_ALFACLICK         = 'alfaclick';
    const METHOD_BANK_TRANSFER     = 'bank_transfer';
    const METHOD_TERMINAL_EUROSET  = 'terminal_euroset';
    const METHOD_TERMINAL_SVYAZNOY = 'terminal_svyaznoy';
    const METHOD_WEBMONEY          = 'webmoney';

    const PAYMENT_METHODS = [
        self::METHOD_OTHER,
        self::METHOD_CREDIT_CARD,
        self::METHOD_YANDEX_MONEY,
        self::METHOD_SBERBANK_ONLINE,
        self::METHOD_MOBILE_MTS,
        self::METHOD_MOBILE_BEELINE,
        self::METHOD_MOBILE_TELE2,
        self::METHOD_MOBILE_MEGAFON,
        self::METHOD_QIWI_WALLET,
        self::METHOD_MONETA_RU,
        self::METHOD_ALFACLICK,
        self::METHOD_BANK_TRANSFER,
        self::METHOD_TERMINAL_EUROSET,
        self::METHOD_TERMINAL_SVYAZNOY,
        self::METHOD_WEBMONEY,
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\models;

use app\entity\Currency;
use app\models\query\UserQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Модель для таблицы "user".
 *
 * @property int $id
 * @property string $username
 * @property int $currency
 * @property string $created_at
 * @property int $is_deleted
 */
class User extends AbstractModel
{
    /**
     * Статусы
     */
    const NOT_DELETED = 0;
    const DELETED     = 1;

    const COL_ID          = 'id';
    const COL_USERNAME    = 'username';
    const COL_CURRENCY    = 'currency';
    const COL_DATE_CREATE = 'created_at';
    const COL_IS_DELETED  = 'is_deleted';

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [self::COL_IS_DELETED, 'boolean',],
            [[self::COL_USERNAME, self::COL_CURRENCY], 'required',],
            [self::COL_USERNAME, 'unique',],
            [self::COL_DATE_CREATE, 'safe',],
            [self::COL_USERNAME, 'string', 'min' => 2, 'max' => 55],
            [self::COL_CURRENCY, 'integer'],
        ];
    }

    public function behaviors(): array
    {
        return
            [
                [
                    'class'              => TimestampBehavior::class,
                    'updatedAtAttribute' => false,
                    'value'              => function () {
                        return date('Y-m-d H:i:s', time());
                    },
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            self::COL_ID          => 'ID',
            self::COL_USERNAME    => 'Имя пользователя',
            self::COL_CURRENCY    => 'Валюта пользователя',
            self::COL_DATE_CREATE => 'Дата создания',
            self::COL_IS_DELETED  => 'Удален ли пользователь',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints(): array
    {
        return $this->attributeLabels();
    }

    /**
     * @inheritdoc
     * @return UserQuery активный запрос используемый AR классом.
     */
    public static function find(): UserQuery
    {
        return new UserQuery(static::class);
    }

    /**
     * Находит идентификатор пользователя по ID.
     *
     * @param string|int $id идентификатор пользователя
     *
     * @return User|null
     * Null возвращается в случае, если идентификатор пользователя не найден
     * или он находится в не активном состоянии или выставлен флаг удален (например activate=0 и/или deleted=1)
     */
    public static function findIdentity($id): ?User
    {
        if (Yii::$app->getSession()->has('user-' . $id)) {
            return new self(Yii::$app->getSession()->get('user-' . $id));
        }

        return self::findOne([
            self::COL_ID         => $id,
            self::COL_IS_DELETED => self::NOT_DELETED,
        ]);
    }

    /**
     * @return Currency
     *
     * @throws \InvalidArgumentException если не корректная валюта
     */
    public function getCurrency(): Currency
    {
        return Currency::create($this->currency);
    }

    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency->getValue();
    }

    public function __toString()
    {
        return (string)$this->username;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\entity;

interface CurrencyInterface
{
    const USD = 1; // Американский доллар
    const EUR = 2; // Евро
    const RUB = 3; // Российский рубль
    const BYN = 4; // Белорусский рубль
    const UAH = 5; // Украинская гривна
    const KZT = 6; // Казахский тенге
    const INR = 7; // Индийская рупия
    const IDR = 8; // Рупия, Индонезия
    const PHP = 9; // Песо, Филиппины
    const THB = 10; // Бат, Таиланд
    const VND = 11; // Вьетнамский донг
    const TRY = 12; // Турецкая лира
    const PLN = 13; // Польский злотый
    const BRL = 14; // Бразильский реал
    const COP = 15; // Колумбийское песо
    const MXN = 16; // Мексиканское песо
    const ZAR = 17; // Ранд, Южная Африка
    const CLP = 18; // Чилийское песо
    const PEN = 19; // Новый соль, Перу
    const AED = 20; // Дирхам (ОАЭ)
    const AZN = 21; // Манат (Азербайджан)
    const CZK = 22; // Чешская крона
    const DKK = 23; // Датская крона
    const GBP = 24; // Фунт стерлингов (Великобритания)
    const IRR = 25; // Иранский реал
    const ILS = 26; // Новый израильский шекель
    const HRK = 27; // Хорватская куна
    const HUF = 28; // Венгерский форинт
    const JPY = 29; // Японская иена
    const GEL = 30; // Грузинский лари
    const MKD = 31; // Македонский денар
    const MNT = 32; // Монгольский тугрик
    const NOK = 33; // Норвежская крона
    const RON = 34; // Новый румынский лей
    const RSD = 35; // Сербский динар
    const SEK = 36; // Шведская крона
    const CNY = 37; // Юань
    const TWD = 38; // Новый тайваньский доллар
    const BDT = 39; // Бангладешская така
    const KHR = 40; // Риель (Камбоджа)
    const KRW = 41; // Вона (Корея)
    const MYR = 42; // Малайзийский ринггит
    const BND = 43; // Брунейский доллар
    const SGD = 44; // Сингапурский доллар
    const PKR = 45; // Пакистанская рупия
    const UZS = 46; // Узбекский сум
    const GHS = 47; // Ганский седи
    const KES = 48; // Кенийский шиллинг
    const TZS = 49; // Танзанийский шиллинг
    const XAF = 50; // Франк КФА BEAC (Габон, Камерун, Республика Конго, ЦАР, Чад, Экваториальная Гвинея)
    const UGX = 51; // Угандийский шиллинг
    const NGN = 52; // Нигерийская найра
    const RWF = 53; // Франк Руанды
    const XOF = 54; // Франк КФА ВСЕАО (Бенин, Кот д'Ивуар, Сенегал, Буркина-Фасо, Гвинея-Бисау, Мали, Нигер, Того)

    const CURRENCY_CODES = [
        self::RUB => 'RUB',
        self::USD => 'USD',
        self::EUR => 'EUR',
        self::BYN => 'BYN',
        self::UAH => 'UAH',
        self::KZT => 'KZT',
        self::INR => 'INR',
        self::IDR => 'IDR',
        self::PHP => 'PHP',
        self::THB => 'THB',
        self::VND => 'VND',
        self::TRY => 'TRY',
        self::PLN => 'PLN',
        self::BRL => 'BRL',
        self::COP => 'COP',
        self::MXN => 'MXN',
        self::ZAR => 'ZAR',
        self::CLP => 'CLP',
        self::PEN => 'PEN',
        self::AED => 'AED',
        self::AZN => 'AZN',
        self::CZK => 'CZK',
        self::DKK => 'DKK',
        self::GBP => 'GBP',
        self::IRR => 'IRR',
        self::ILS => 'ILS',
        self::HRK => 'HRK',
        self::HUF => 'HUF',
        self::JPY => 'JPY',
        self::GEL => 'GEL',
        self::MKD => 'MKD',
        self::MNT => 'MNT',
        self::NOK => 'NOK',
        self::RON => 'RON',
        self::RSD => 'RSD',
        self::SEK => 'SEK',
        self::CNY => 'CNY',
        self::TWD => 'TWD',
        self::BDT => 'BDT',
        self::KHR => 'KHR',
        self::KRW => 'KRW',
        self::MYR => 'MYR',
        self::BND => 'BND',
        self::SGD => 'SGD',
        self::PKR => 'PKR',
        self::UZS => 'UZS',
        self::GHS => 'GHS',
        self::KES => 'KES',
        self::TZS => 'TZS',
        self::XAF => 'XAF',
        self::UGX => 'UGX',
        self::NGN => 'NGN',
        self::RWF => 'RWF',
        self::XOF => 'XOF',
    ];
}

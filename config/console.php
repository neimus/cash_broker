<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id'                  => 'user-balance-console',
    'name'                => 'Микросервис баланса пользователей',
    'language'            => 'ru',
    'sourceLanguage'      => 'ru-RU',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'queueJob', 'queueEvent'],
    'controllerNamespace' => 'app\commands',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        'cache'             => [
            'class' => 'yii\caching\FileCache',
        ],
        'log'               => [
            'targets' => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/app.console.log',
                    'levels'  => ['error', 'warning'],
                ],
            ],
        ],
        'db'                => $db,
        'mutex'             => [
            'class' => 'yii\mutex\MysqlMutex',
        ],
        'financeManager'    => [
            'class' => 'app\components\FinanceManager',
        ],
        'currencyConverter' => [
            'class'                     => 'app\components\CurrencyConverter',
            'classCurrencyRateProvider' => 'app\components\rates\DbCurrencyRatesProvider',
        ],
        'queueJob'          => [
            'class'         => \yii\queue\amqp_interop\Queue::class,
            'ttr'           => 5 * 60, // Макимальное время жизни
            'attempts'      => 5, // Максимальное кол-во попыток при ошибках
            'exchangeName'  => 'exchange_job',
            'queueName'     => 'queue_job',
            'driver'        => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'strictJobType' => false,
            'serializer'    => app\workflow\JobSerializer::class,
        ],
        'queueEvent'        => [
            'class'         => \yii\queue\amqp_interop\Queue::class,
            'ttr'           => 5 * 60, // Макимальное время жизни
            'attempts'      => 5, // Максимальное кол-во попыток при ошибках
            'exchangeName'  => 'exchange_event',
            'queueName'     => 'queue_event',
            'driver'        => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'strictJobType' => false,
            'serializer'    => app\workflow\EventSerializer::class,
        ],
    ],
    'params'              => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

require __DIR__ . '/queue.php';

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

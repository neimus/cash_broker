<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow;

use app\helpers\ClassHelper;
use yii\queue\serializers\JsonSerializer;

class JobSerializer extends JsonSerializer
{
    const JOB_NAMESPACE = 'app\workflow\job';

    /**
     * @var string
     */
    public static $commandKey = 'command';

    /**
     * @var string
     */
    public static $suffixClass = 'Job';

    protected function fromArray($data)
    {
        if (\is_array($data) && isset($data[self::$commandKey])) {
            $class = ClassHelper::getNamespaceByShortName(self::JOB_NAMESPACE, $data[self::$commandKey],
                self::$suffixClass);
            if (class_exists($class)) {
                $data[$this->classKey] = $class;
                unset($data[self::$commandKey]);
            }
        }

        return parent::fromArray($data);
    }
}
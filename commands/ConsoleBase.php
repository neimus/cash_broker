<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.17
 */

namespace app\commands;

use app\exception\console\FileNotFoundException;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\console\Application;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;

/**
 *
 * @property array $yiiConfiguration
 */
class ConsoleBase extends Controller
{
    const APP_DIR = __DIR__ . '/../';

    private $appConfig;
    private $appConfigTest;
    private $charset;

    /**
     * Возвращает конфигурацию Yii и устанавливается кодировка консоли
     *
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\base\InvalidParamException
     */
    public function beforeAction($action): bool
    {
        try {
            $this->appConfig = $this->getYiiConfiguration();
            $this->appConfigTest = $this->loadYiiConfiguration('test');
            $this->appConfig['id'] = 'temp';
            $this->charset = $this->setEncoding();

        } catch (\InvalidArgumentException $exception) {
            $this->logConsole($exception->getMessage(), false);
        }

        return parent::beforeAction($action);
    }

    /**
     * Возвращает конфигурацию Yii
     *
     * TODO https://github.com/yiisoft/yii2/pull/5687
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    protected function getYiiConfiguration(): array
    {
        if (isset($GLOBALS['config'])) {
            $config = $GLOBALS['config'];
        } else {
            $config = $this->loadYiiConfiguration();
        }

        return $config;
    }

    /**
     * @param string $env
     *
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    private function loadYiiConfiguration(string $env = 'console'): array
    {
        return require Yii::getAlias('@app') . "/config/$env.php";
    }

    /**
     * Установка кодировки в зависимости от ОС
     *
     * @return string
     */
    private function setEncoding(): string
    {
        return strtolower(stripos(PHP_OS, 'win') === 0 ? 'cp866' : 'utf-8');
    }

    /**
     * @param string $message
     * @param bool $success
     */
    public function logConsole($message, $success = null): void
    {
        if ($success === null) {
            $this->stdout($message, Console::FG_BLUE, Console::BOLD);
        } elseif ($success) {
            $this->stdout('Success! ' . $message, Console::FG_GREEN, Console::BOLD);
        } else {
            $this->stderr('Error! ' . $message, Console::FG_RED, Console::BOLD);
        }
        echo PHP_EOL;
    }

    /**
     * @inheritdoc
     *
     * @param string $string
     *
     * @return bool|int
     */
    public function stdout($string)
    {
        $string = iconv('utf-8', $this->charset . '//IGNORE', $string);
        if ($this->isColorEnabled()) {
            $args = \func_get_args();
            array_shift($args);
            $string = Console::ansiFormat($string, $args);
        }

        return Console::stdout($string);
    }

    /**
     * @inheritdoc
     *
     * @param string $string
     *
     * @return bool|int
     */
    public function stderr($string)
    {
        $string = iconv('utf-8', $this->charset . '//IGNORE', $string);
        if ($this->isColorEnabled(\STDERR)) {
            $args = \func_get_args();
            array_shift($args);
            $string = Console::ansiFormat($string, $args);
        }

        return fwrite(\STDERR, $string);
    }

    /**
     * @param string $route
     * @param array $params
     * @param array|null $appConfig
     *
     * @return bool
     */
    public function runCommand(string $route, array $params = array(), $appConfig = null): bool
    {
        $ok = false;
        try {
            $app = Yii::$app;
            $temp = new Application($appConfig ?? $this->appConfig);
            $temp->runAction(ltrim($route, '/'), $params);
            unset($temp);
            Yii::$app = $app;
            Yii::$app->log->logger->flush(true);
            $ok = true;
        } catch (InvalidConfigException $exception) {
            $this->logConsole($exception->getMessage(), false);
        } catch (Exception $exception) {
            $this->logConsole($exception->getMessage(), false);
        }

        return $ok;
    }

    /**
     * @param string $route
     * @param array $params
     *
     * @return bool
     */
    public function runCommandInEnvTest(string $route, array $params = array()): bool
    {
        return $this->runCommand($route, $params, $this->appConfigTest);
    }

    /**
     * @param $pathSource
     * @param $pathDestination
     *
     * @throws ExitException
     * @throws FileNotFoundException
     */
    public function copyFile($pathSource, $pathDestination): void
    {
        $dirFileSource = self::APP_DIR . $pathSource;
        $dirFileDestination = self::APP_DIR . $pathDestination;
        if ($this->issetFile($pathSource)) {
            if (copy($dirFileSource, $dirFileDestination)) {
                $this->logConsole(sprintf('Файл %s успешно скопирован в %s', $pathSource, $pathDestination), true);
            } else {
                throw new ExitException(sprintf('Не удалось скопировать файл %s в %s', $pathSource, $pathDestination));
            }
        } else {
            throw new FileNotFoundException(sprintf('%s файл не найден', $pathSource));
        }
    }

    public function issetFile(string $path): bool
    {
        return is_file(self::APP_DIR . $path);
    }

    public function removeFile(string $path): bool
    {
        return unlink(self::APP_DIR . $path);
    }

    /**
     * @param string $path
     *
     * @return array
     * @throws FileNotFoundException
     */
    public function loadDataJson(string $path): array
    {
        if ($this->issetFile($path)) {
            return json_decode(file_get_contents(self::APP_DIR . $path), true);
        }

        throw new FileNotFoundException(sprintf('Файл не найден: %s', self::APP_DIR . $path));
    }
}
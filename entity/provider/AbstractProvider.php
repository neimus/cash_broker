<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\entity\provider;

use app\models\PaymentMethodInterface;

abstract class AbstractProvider implements PaymentProviderInterface, PaymentMethodInterface
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $externalId;


    /**
     * @var string
     */
    protected $rawData = '';

    /**
     * @return int
     */
    abstract public function getCodeProvider(): int;

    /**
     * @return string
     */
    public function getRawData(): string
    {
        return $this->rawData;
    }

    /**
     * @param string $data
     *
     * @return $this
     */
    public function setRawData(string $data): self
    {
        $this->rawData = $data;

        return $this;
    }

    /**
     * @return string[32]
     */
    public function getDetailHash(): string
    {
        return md5($this->rawData);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return self
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     *
     * @return self
     */
    public function setExternalId(string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function __toString()
    {
        return sprintf('{%s: %s}', self::PAYMENT_PROVIDERS[static::getCodeProvider()], static::getRawData());
    }

}
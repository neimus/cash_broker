<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow;

use app\helpers\YiiComponent;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\queue\JobInterface;

/**
 * @property string $commandName
 */
abstract class AbstractJob extends BaseObject implements JobInterface
{
    /**
     * @param array $data
     *
     * @return array
     * @throws \ReflectionException
     */
    public static function package(array $data = []): array
    {
        $classShortName = (new \ReflectionClass(static::class))->getShortName();
        $className = preg_replace('/' . JobSerializer::$suffixClass . '$/', '', $classShortName);

        return ArrayHelper::merge([JobSerializer::$commandKey => $className], $data);
    }

    abstract protected function getContext(): array;

    abstract protected static function getEventName(): string;

    protected function logErrorMessage(\Exception $exception)
    {
        \Yii::error(sprintf('%s Context: %s', $exception->getMessage(), json_encode($this->getContext())),
            static::class);
    }

    protected function sendSuccessEvent(array $data)
    {
        YiiComponent::getQueueEvent()->push([
            'command' => static::getEventName(),
            'status'  => true,
            'error'   => '',
            'data'    => $data,
            'context' => $this->getContext(),
        ]);
    }

    protected function sendErrorEvent($errors)
    {
        YiiComponent::getQueueEvent()->push([
            'command' => static::getEventName(),
            'status'  => false,
            'error'   => $errors,
            'data'    => [],
            'context' => $this->getContext(),
        ]);
    }
}
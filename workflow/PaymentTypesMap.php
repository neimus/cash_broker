<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 02.04.18
 */

namespace app\workflow;

use app\entity\provider\AbstractProvider;
use app\entity\provider\PaymentProviderInterface;
use app\helpers\ClassHelper;
use app\models\PaymentMethodInterface;
use yii\base\InvalidConfigException;

class PaymentTypesMap implements PaymentMethodInterface, PaymentProviderCodeInterface
{
    const PROVIDER_NAMESPACE = 'app\entity\provider';

    public static $suffixClassProvider = 'Provider';

    private static $map = [
        self::METHOD_CREDIT_CARD       => [
            self::PROVIDER_CODE_ECOMMPAY_TYPE_CREDIT_CARD,
            self::PROVIDER_CODE_PAYANYWAY_TYPE_CREDIT_CARD,
            self::PROVIDER_CODE_FATPAY_TYPE_CREDIT_CARD,
        ],
        self::METHOD_YANDEX_MONEY      => [
            self::PROVIDER_CODE_ECOMMPAY_TYPE_YANDEX_MONEY,
            self::PROVIDER_CODE_PAYANYWAY_TYPE_YANDEX_MONEY,
        ],
        self::METHOD_SBERBANK_ONLINE   => [
            self::PROVIDER_CODE_ECOMMPAY_TYPE_SBERBANK_ONLINE,
        ],
        self::METHOD_MOBILE_MTS        => [
            self::PROVIDER_CODE_WINPAY_TYPE_MTS,
        ],
        self::METHOD_MOBILE_MEGAFON    => [
            self::PROVIDER_CODE_WINPAY_TYPE_MEGAFON,
        ],
        self::METHOD_MOBILE_BEELINE    => [
            self::PROVIDER_CODE_WINPAY_TYPE_BEELINE,
        ],
        self::METHOD_MOBILE_TELE2      => [
            self::PROVIDER_CODE_WINPAY_TYPE_TELE2,
        ],
        self::METHOD_QIWI_WALLET       => [
            self::PROVIDER_CODE_ECOMMPAY_TYPE_QIWI_WALLET,
            self::PROVIDER_CODE_PAYANYWAY_TYPE_QIWI_WALLET,
        ],
        self::METHOD_MONETA_RU         => [
            self::PROVIDER_CODE_PAYANYWAY_TYPE_MONETA_RU,
        ],
        self::METHOD_ALFACLICK         => [
            self::PROVIDER_CODE_PAYANYWAY_TYPE_ALFA_CLICK,
        ],
        self::METHOD_BANK_TRANSFER     => [
            self::PROVIDER_CODE_PAYANYWAY_TYPE_WIRE_TRANSFER,
        ],
        self::METHOD_TERMINAL_EUROSET  => [
            self::PROVIDER_CODE_WINPAY_TYPE_TERMINAL_EUROSET,
        ],
        self::METHOD_TERMINAL_SVYAZNOY => [
            self::PROVIDER_CODE_WINPAY_TYPE_TERMINAL_SVYAZNOY,
        ],
        self::METHOD_WEBMONEY          => [
            self::PROVIDER_CODE_ECOMMPAY_TYPE_WEBMONEY,
        ],
    ];

    /**
     * @return array|string[]
     */
    public static function getMethods(): array
    {
        return array_keys(self::$map);
    }

    /**
     * @param string $method
     *
     * @return array|null
     */
    public static function getProviderTypes(string $method): ?array
    {
        if (array_key_exists($method, self::$map)) {
            return self::$map[$method];
        }

        return null;
    }

    /**
     * @param int $providerCode
     *
     * @return null|string
     */
    public static function getMethod(int $providerCode): ?string
    {
        foreach (self::$map as $method => $types) {
            if (\in_array($providerCode, $types, true)) {
                return $method;
            }
        }

        return null;
    }

    /**
     * @param int $paymentCode
     *
     * @return AbstractProvider|object
     * @throws InvalidConfigException
     */
    public static function getProviderByPaymentCode(int $paymentCode)
    {
        $paymentProvider = floor($paymentCode / 100) * 100;

        if (isset(PaymentProviderInterface::PAYMENT_PROVIDERS[$paymentProvider])) {
            $classNamespace = ClassHelper::getNamespaceByShortName(
                self::PROVIDER_NAMESPACE, PaymentProviderInterface::PAYMENT_PROVIDERS[$paymentProvider],
                self::$suffixClassProvider
            );

            return \Yii::createObject($classNamespace);
        }

        return null;
    }
}
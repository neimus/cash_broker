<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\entity\provider;

class OtherProvider extends AbstractProvider
{
    /**
     * @return int
     */
    public function getCodeProvider(): int
    {
        return self::PAYMENT_PROVIDER_OTHER;
    }
}
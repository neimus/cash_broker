<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 02.04.18
 */

namespace app\tests\unit;

use app\entity\Currency;
use app\entity\Money;
use app\exception\console\FileNotFoundException;
use app\exception\finance\FinanceManagerException;
use app\exception\finance\FinanceTransferInvalidArgument;
use app\exception\finance\TransactionException;
use app\helpers\YiiComponent;
use app\models\CurrencyRate;
use app\models\Payout;
use app\models\Refill;
use app\models\Transaction;
use app\models\User;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use yii\base\UserException;

class BaseTestCase extends TestCase
{
    const FILE_DATA__USERS        = __DIR__ . '/../../tests/data/demo_users.json';
    const FILE_DATA__CURENCY_RATE = __DIR__ . '/../../tests/data/demo_currency_rate.json';

    protected static $testUsers = [];

    /**
     * @var User[]
     */
    private static $users;

    /**
     * @throws \yii\db\Exception
     * @throws \RuntimeException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     * @throws \InvalidArgumentException
     * @throws FileNotFoundException
     * @throws UserException
     * @throws \Exception
     */
    public static function setUpBeforeClass()
    {
        self::$testUsers = self::loadDataJson(self::FILE_DATA__USERS);
        self::clearAll();
        self::createCurrencyRate();
        self::createUsers();
    }

    /**
     * @throws \RuntimeException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     * @throws \InvalidArgumentException
     * @throws UserException
     * @throws \Exception
     */
    protected function setUp()
    {
        self::makeRefillUsers();
    }

    /**
     * @throws \yii\db\Exception
     * @throws \RuntimeException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    protected function tearDown()
    {
        self::clearFinance();
    }

    /**
     * @throws \yii\db\Exception
     */
    public static function tearDownAfterClass()
    {
        self::clearAll();
    }

    /**
     * @param string $path
     *
     * @return array
     * @throws FileNotFoundException
     */
    protected static function loadDataJson(string $path): array
    {
        if (is_file($path)) {
            return json_decode(file_get_contents($path), true);
        }

        throw new FileNotFoundException(sprintf('Файл не найден: %s', $path));
    }

    protected static function getUserByName($username)
    {
        if (self::$users === null) {
            self::$users = User::find()->indexBy(User::COL_USERNAME)->all();
        }

        return self::$users[$username] ?? null;
    }

    /**
     * @param int $index
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    protected static function getTestUserBalanceByIndex(int $index): Money
    {
        if (isset(self::$testUsers[$index])) {
            return Money::createFromString(sprintf('%s %s', self::$testUsers[$index]['currency'],
                self::$testUsers[$index]['amount']));
        }

        throw new InvalidArgumentException('Не найден тестовый пользователь с индексом ' . $index);
    }

    /**
     * Создает тестовых пользователей
     *
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     * @throws \InvalidArgumentException
     * @throws UserException
     * @throws \Exception
     */
    protected static function createUsers(): void
    {
        foreach (self::$testUsers as $testUser) {
            $user = new User([
                'username'   => $testUser['username'],
                'is_deleted' => User::NOT_DELETED,
            ]);
            $user->setCurrency(Currency::createByCode($testUser['currency']));
            if (!$user->save()) {
                throw new UserException(sprintf('User %s: %s', $testUser['username'], $user->getModelErrors()));
            }
        }
    }

    /**
     * @throws FileNotFoundException
     * @throws \RuntimeException
     */
    protected static function createCurrencyRate(): void
    {
        $dataCurrencyRates = self::loadDataJson(self::FILE_DATA__CURENCY_RATE);
        foreach ($dataCurrencyRates as $dataCurrencyRate) {
            $currencyRate = new CurrencyRate([
                'currency_from' => $dataCurrencyRate['currency_from'],
                'currency_to'   => $dataCurrencyRate['currency_to'],
                'ratio'         => $dataCurrencyRate['ratio'],
            ]);
            if (!$currencyRate->save()) {
                throw new \RuntimeException($currencyRate->getModelErrors());
            }
        }
    }

    /**
     * @throws \app\exception\finance\CurrencyRateNotFoundException
     * @throws \yii\base\UserException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     * @throws \InvalidArgumentException
     * @throws UserException
     * @throws \Exception
     */
    protected static function makeRefillUsers(): void
    {
        foreach (self::$testUsers as $demoUser) {
            $user = User::find()->getByUsername($demoUser['username']);
            if ($user === null) {
                throw new UserException(sprintf('User %s not found', $demoUser['username']));
            }

            $amount = Money::create($user->getCurrency(), $demoUser['amount']);
            YiiComponent::getFinanceManager()->makeCorrection($user, $amount, 'demo refill');
        }
    }

    /**
     * @throws \yii\db\Exception
     */
    protected static function clearAll(): void
    {
        self::clearFinance();
        self::clearCurrencyRate();
        self::clearUsers();
    }

    /**
     * @throws \yii\db\Exception
     */
    protected static function clearFinance(): void
    {
        \Yii::$app->db->createCommand()->delete(Transaction::tableName())->execute();
        \Yii::$app->db->createCommand()->delete(Refill::tableName())->execute();
        \Yii::$app->db->createCommand()->delete(Payout::tableName())->execute();
    }

    /**
     * @throws \yii\db\Exception
     */
    protected static function clearCurrencyRate(): void
    {
        \Yii::$app->db->createCommand()->delete(CurrencyRate::tableName())->execute();
    }

    /**
     * @throws \yii\db\Exception
     */
    protected static function clearUsers(): void
    {
        \Yii::$app->db->createCommand()->delete(User::tableName())->execute();
    }

}
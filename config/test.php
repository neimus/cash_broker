<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
$config = [
    'id'             => 'user-balance-tests',
    'basePath'       => dirname(__DIR__),
    'aliases'        => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language'       => 'ru',
    'sourceLanguage' => 'ru-RU',
    'components'     => [
        'db'                => $db,
        'log'               => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/app.test.log',
                    'levels'  => ['error', 'warning'],
                ],
            ],
        ],
        'mutex'             => [
            'class' => 'yii\mutex\MysqlMutex',
        ],
        'financeManager'    => [
            'class' => 'app\components\FinanceManager',
        ],
        'currencyConverter' => [
            'class'                     => 'app\components\CurrencyConverter',
            'classCurrencyRateProvider' => 'app\components\rates\DbCurrencyRatesProvider',
        ],
        'queueJob'          => [
            'class'         => \yii\queue\amqp_interop\Queue::class,
            'ttr'           => 5 * 60, // Макимальное время жизни
            'attempts'      => 5, // Максимальное кол-во попыток при ошибках
            'exchangeName'  => 'exchange_job',
            'queueName'     => 'queue_job',
            'driver'        => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'strictJobType' => false,
            'serializer'    => app\workflow\JobSerializer::class,
        ],
        'queueEvent'        => [
            'class'         => \yii\queue\amqp_interop\Queue::class,
            'ttr'           => 5 * 60, // Макимальное время жизни
            'attempts'      => 5, // Максимальное кол-во попыток при ошибках
            'exchangeName'  => 'exchange_event',
            'queueName'     => 'queue_event',
            'driver'        => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'strictJobType' => false,
            'serializer'    => app\workflow\EventSerializer::class,
        ],
        // 'mailer' => [
        //     'useFileTransport' => true,
        // ],
        'assetManager'      => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'request'           => [
            'cookieValidationKey'  => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
    ],
    'params'         => $params,
];

require __DIR__ . '/queue.php';

return $config;

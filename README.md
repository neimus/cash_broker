<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Микросервис баланса пользователей</h1>
    <br>
</p>

Приложение хранит в себе идентификаторы пользователей и их баланс. Взаимодействие с ним
осуществляется исключительно с помощью брокера очередей.
По требованию внешней системы (общение между микросервисами через очередь сообщений),
микросервис может выполнить одну из следующих операций со счетом пользователя:
* Списание
* Зачисление
* Блокирование с последующим списанием или
разблокированием. Заблокированные средства недоступны для использования. Блокировка
означает что некая операция находится на авторизации и ждет какого-то внешнего
подтверждения, ее можно впоследствии подтвердить или отклонить
* Перевод от пользователя к пользователю

После проведения любой из этих операций генерируется событие.

Основные требования к воркерам:
* Код воркеров должен безопасно выполняться параллельно в разных процессах (RabbitMQ и supervisor);
* Воркеры могут запускаться одновременно в любом числе экземпляров и
выполняться произвольное время ([yii2-queue](https://github.com/yiisoft/yii2-queue));
* Все операции должны обрабатываться корректно, без двойных списаний (mutex MySQL)
* Покрытие кода юнит-тестами (только класс FinanceManager).


[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             
      commands/
      components/           
      config/
      entity/
      event/
      exception/
      helpers/                   
      mail/    
      migrations/           
      models/             
      runtime/            
      tests/              
      vendor/             
      views/              
      web/  
      widgets/
      workflow/              

REQUIREMENTS
------------

* Язык программирования: PHP >= 7 (сконфигурирован с включенной настройкой--enable-bcmath), стандарт кодирования - PSR-2
* База данных: MySQL >= 5.7.5
* Брокер очередей: RabbitMQ
* Монитор процессов для linux: Supervisor
* Разное: Nginx, Composer

INSTALLATION
------------

* Установите rabbitmq и запустите его как сервис (настройки находятся в файле `config/queue.php.dist`)
* Установите supervisor для контроля и запуска команды для обработки очередей `sudo apt-get install supervisor` (подробнее [Worker starting control](https://github.com/yiisoft/yii2-queue/blob/master/docs/guide/worker.md))
* Настройте supervisor `/etc/supervisor/conf.d`:
```conf
[program:yii-queue-worker]
process_name=%(program_name)s_%(process_num)02d
command=/usr/bin/php /var/www/my_project/yii queue-job/listen --verbose=1 --color=0
autostart=true
autorestart=true
user=www-data
numprocs=4
redirect_stderr=true
stdout_logfile=/var/www/my_project/log/yii-queue-worker.log
```
* Настройте соединение с БД MySQL (настройки находятся в файле `config/db.php.dist`)
* Установите composer и запустите его из проекта (`composer install`)
* Инициализируйте (скопируются параметры, обновиться composer и применяться все миграции) проект командой `php yii init` (для создания демо-данных `php yii init/demo`)
* Запустите supervisor или команду для обработки очередей `php yii queue-job/listen` (подробнее [Worker starting control](https://github.com/yiisoft/yii2-queue/blob/master/docs/guide/worker.md))

AMQP сообщения
----
Exchange `exchange_job` - получает запросы для обработки
Exchange `exchange_event` - создает события после обработки запроса. Структура сообщения:
```json
{
  "command": "{Наименование события}",
  "status": true|false,
  "error": "{Сообщение об ошибке}",
  "data": {}, // Дополнительные данные
  "context": {} // Данные, которые были переданы в запросе
}

```

<h4>Зачисление на счет</h4>

* Создания запроса на зачисления (при этом сумма фактически не зачисляется)

Query `exchange_job`:
```json
{
  "toUserId": 1, // Идентификатор пользователя для которого производится зачисление
  "amount": "RUB 1352.50", // Сумма для зачисления в виде строки
  "paymentCode": 101, // Код провайдера с помощью которого производиться зачисление см. app\workflow\PaymentProviderCodeInterface
  "rawData": "", // Данные от провайдера о платеже в виде строки
  "externalPaymentId": "ext1215454", // Уникальный идентификатор платежа для данного провайдера
  "status": "new", // Статус зачисления
}
```
Event `exchange_event`:
```json
{
  "command": "refill_event",
  "status": true|false,
  "error": "{Сообщение об ошибке}",
  "data": {"refillId": 1}, // Идентификатор зачисления
  "context": {...} // Данные, которые были переданы в запросе
}
```

* Изменение статуса зачисление (в ожидании)

Query `exchange_job`:
```json
{
  "refillId": 1, // Идентификатор зачисления
  "status": "waiting", // Статус зачисления
}
```
Event `exchange_event`:
```json
{
  "command": "refill_event",
  "status": true|false,
  "error": "{Сообщение об ошибке}",
  "data": {},
  "context": {...} // Данные, которые были переданы в запросе
}
```

* Подтверждение зачисления (происходит фактическое зачисление на счет)

Query `exchange_job`:
```json
{
  "refillId": 1, // Идентификатор зачисления
  "status": "approved", // Статус зачисления
}
```
Event `exchange_event`:
```json
{
  "command": "refill_event",
  "status": true|false,
  "error": "{Сообщение об ошибке}",
  "data": {},
  "context": {...} // Данные, которые были переданы в запросе
}
```

* Откат зачисления (происходит фактическое списание зачисленной суммы со счета)

Query `exchange_job`:
```json
{
  "refillId": 1, // Идентификатор зачисления
  "status": "declined", // Статус зачисления
}
```
Event `exchange_event`:
```json
{
  "command": "refill_event",
  "status": true|false,
  "error": "{Сообщение об ошибке}",
  "data": {},
  "context": {...} // Данные, которые были переданы в запросе
}
```



TODO
----

* Документировать amqp сообщения
* Покрыть тестами workflow
* Для Refill и Payout `payment_method` вынести в отдельную таблицу или использовать численное представление
* Добавить повторный запуск для задачи при mutex


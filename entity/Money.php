<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\entity;

class Money
{
    const SCALE = 4;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var string
     */
    private $amount;

    /**
     * @param Currency $currency
     * @param string|int|float|null $amount
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(Currency $currency, $amount)
    {
        $amount = (string)($amount ?? '0');

        if (!self::isValid($amount)) {
            throw new \InvalidArgumentException("'$amount' не корректная сумма для денег");
        }

        $this->currency = $currency;
        $this->amount = $amount;
    }

    /**
     * @param Currency $currency
     * @param string|int|float|null $amount
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public static function create(Currency $currency, $amount): Money
    {
        return new self($currency, $amount);
    }

    /**
     * @param $amount
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public static function RUB($amount): Money
    {
        return new self(Currency::create(Currency::RUB), $amount);
    }

    /**
     * @param string $amount
     *
     * @return bool
     */
    public static function isValid($amount): bool
    {
        return preg_match('/^-?\d{1,13}(\.\d{1,4})?$/', $amount);
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->amount;
    }

    /**
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function getNegative(): Money
    {
        return new self($this->currency, bcmul($this->amount, '-1', self::SCALE));
    }

    /**
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function getAbsolute(): Money
    {
        return new self($this->currency,
            bccomp($this->amount, 0, self::SCALE) >= 0
                ? $this->amount
                : bcmul($this->amount, '-1', self::SCALE));
    }

    /**
     * @param Money $other
     *
     * @return int
     *  -1: other больше
     *  0: равны
     *  1: текущее больше
     *
     * @throws \InvalidArgumentException
     */
    public function compareTo(Money $other): int
    {
        if (!$this->currency->equalsTo($other->currency)) {
            throw new \InvalidArgumentException('Деньги с разными валютами нельзя сравнивать');
        }

        return bccomp($this->amount, $other->amount, self::SCALE);
    }

    /**
     * @param Money $other
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function equalTo(Money $other): bool
    {
        return $this->compareTo($other) === 0;
    }

    /**
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function equalToZero(): bool
    {
        return $this->compareTo(new self($this->currency, '0')) === 0;
    }

    /**
     * @param Money $other
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function greaterThan(Money $other): bool
    {
        return $this->compareTo($other) > 0;
    }

    /**
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function greaterThanZero(): bool
    {
        return $this->compareTo(new self($this->currency, '0')) > 0;
    }

    /**
     * @param Money $other
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     */
    public function greaterThanOrEqual(Money $other): bool
    {
        return $this->compareTo($other) >= 0;
    }

    /**
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function greaterThanOrEqualZero(): bool
    {
        return $this->compareTo(new self($this->currency, '0')) >= 0;
    }

    /**
     * @param Money $money
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function subtract(Money $money): Money
    {
        if (!$this->getCurrency()->equalsTo($money->getCurrency())) {
            throw new \InvalidArgumentException('Арифметические операции не могут применяться к объектам Money с разными валютами');
        }

        return new self($this->getCurrency(), bcsub($this->amount, $money->getValue(), self::SCALE));
    }

    /**
     * @param Money $money
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function add(Money $money): Money
    {
        if (!$this->getCurrency()->sameAs($money->getCurrency())) {
            throw new \InvalidArgumentException('Арифметические операции не могут применяться к объектам Money с разными валютами');
        }

        return new self($this->getCurrency(), bcadd($this->amount, $money->getValue(), self::SCALE));
    }

    /**
     * @param string $multiplier
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function multiply(string $multiplier): Money
    {
        if (!self::isValid($multiplier)) {
            throw new \InvalidArgumentException("'$multiplier' недействительный множитель для денег");
        }

        return new self($this->getCurrency(), bcmul($this->amount, $multiplier, self::SCALE));
    }

    /**
     * @param string $divider
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function divide(string $divider): Money
    {
        if ((int)$divider === 0 || !self::isValid($divider)) {
            throw new \InvalidArgumentException("'$divider' недействительный делитель для денег");
        }

        return new self($this->getCurrency(), bcdiv($this->amount, $divider, self::SCALE));
    }

    /**
     * @param int $precision
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function round($precision = 0): Money
    {
        return self::create($this->getCurrency(), round((float)$this->getValue(), $precision));
    }

    /**
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function ceil(): Money
    {
        return self::create($this->getCurrency(), ceil((float)$this->getValue()));
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s %s', $this->getCurrency()->getCode(), $this->amount);
    }

    /**
     * Например RUB 55.51
     *
     * @param string $string
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public static function createFromString(string $string): Money
    {
        if (preg_match('/^([A-Z]{3})\s+(-?\d{1,13}(:?\.\d{1,4})?)$/', $string, $matches)) {
            $currency = Currency::createByCode($matches[1]);

            return self::create($currency, $matches[2]);
        }

        throw new \InvalidArgumentException(sprintf('Неверная сумма: %s', $string));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\commands;

use app\entity\Currency;
use app\entity\Money;
use app\exception\console\FileNotFoundException;
use app\exception\finance\CurrencyRateNotFoundException;
use app\exception\finance\FinanceManagerException;
use app\exception\finance\FinanceTransferInvalidArgument;
use app\exception\finance\TransactionException;
use app\helpers\YiiComponent;
use app\models\CurrencyRate;
use app\models\User;
use Yii;
use yii\base\ExitException;
use yii\base\UserException;
use yii\console\ExitCode;

/**
 * Инициализация приложения
 * @package app\commands
 */
class InitController extends ConsoleBase
{
    const FILE_DEMO_USERS        = 'tests/data/demo_users.json';
    const FILE_DEMO_CURENCY_RATE = 'tests/data/demo_currency_rate.json';

    /**
     * @param string $actionID
     *
     * @return array
     */
    public function options($actionID): array
    {
        if ($actionID === 'index') {
            return ['demo', 'help'];
        }

        return ['help'];
    }

    /**
     * @return array
     */
    public function optionAliases(): array
    {
        return [
            'h' => 'help',
            'd' => 'demo',
        ];
    }

    /**
     * Инициализация проекта. Применяет миграций, обновляет composer
     *
     * @return int
     */
    public function actionIndex(): int
    {
        return $this->initialization(false);
    }

    /**
     * Инициализирует окружение для демонстрации (создает тестовых пользователей)
     *
     * @return int
     */
    public function actionDemo(): int
    {
        return $this->initialization(true);
    }

    /**
     * @param null|bool $isDemo
     *
     * @return int
     */
    private function initialization($isDemo = false): int
    {
        try {
            $this->copyFile('config/params.php.dist', 'config/params.php');
            if (!$this->issetFile('config/db.php')) {
                $this->copyFile('config/db.php.dist', 'config/db.php');
            }
            if (!$this->issetFile('config/queue.php')) {
                $this->copyFile('config/queue.php.dist', 'config/queue.php');
            }
            // exec('composer update');

            if ($this->runCommand('migrate/up',
                    ['interactive' => false]) && $this->runCommandInEnvTest('migrate/up',
                    ['interactive' => false])) {
                if ($isDemo && $this->confirm('Создать демо-пользователей?', true)) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $this->createDemoCurrencyRate();
                        $this->createDemoUsers();
                        $transaction->commit();
                    } catch (\Exception $exception) {
                        $transaction->rollBack();
                        throw $exception;
                    }
                }
            } else {
                throw new ExitException('Не удалось применить миграцию');
            }

            return ExitCode::OK;
        } catch (\Exception $exception) {
            $this->logConsole($exception->getMessage(), false);

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

    /**
     * Создает демо-пользователей
     *
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     * @throws \InvalidArgumentException
     * @throws FileNotFoundException
     * @throws UserException
     * @throws \Exception
     */
    private function createDemoUsers(): void
    {
        $this->logConsole('Добавление пользователей');
        $demoUsers = $this->loadDataJson(self::FILE_DEMO_USERS);
        foreach ($demoUsers as $demoUser) {
            $user = new User([
                'username'   => $demoUser['username'],
                'is_deleted' => User::NOT_DELETED,
            ]);
            $user->setCurrency(Currency::createByCode($demoUser['currency']));
            if ($user->save()) {
                $amount = Money::create($user->getCurrency(), $demoUser['amount']);
                YiiComponent::getFinanceManager()->makeCorrection($user, $amount, 'demo refill');
            } else {
                throw new UserException(sprintf('User %s: %s', $demoUser['username'], $user->getModelErrors()));
            }
        }
    }

    /**
     * @throws FileNotFoundException
     * @throws ExitException
     */
    private function createDemoCurrencyRate(): void
    {
        $this->logConsole('Добавление курсов валют');
        $demoCurrencyRates = $this->loadDataJson(self::FILE_DEMO_CURENCY_RATE);
        foreach ($demoCurrencyRates as $demoCurrencyRate) {
            $currencyRate = new CurrencyRate([
                'currency_from' => $demoCurrencyRate['currency_from'],
                'currency_to'   => $demoCurrencyRate['currency_to'],
                'ratio'         => $demoCurrencyRate['ratio'],
            ]);
            if (!$currencyRate->save()) {
                throw new ExitException($currencyRate->getModelErrors());
            }
        }
    }
}
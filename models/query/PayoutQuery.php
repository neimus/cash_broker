<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.2018 */

namespace app\models\query;

use app\models\Payout;
use yii\db\ActiveQuery;

/**
 * ActiveQuery для [[Payout]].
 *
 * @see Payout
 */
class PayoutQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Payout[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Payout|null
     */
    public function one($db = null): ?Payout
    {
        return parent::one($db);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 02.04.18
 */

namespace app\tests\unit\finance;

use app\entity\Money;
use app\entity\provider\AbstractProvider;
use app\entity\provider\OtherProvider;
use app\helpers\YiiComponent;
use app\models\Refill;
use app\models\Transaction;
use app\tests\unit\BaseTestCase;

class FinanceManagerTest extends BaseTestCase
{
    /**
     * @throws \Exception
     */
    public function testGetBalance(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $balance = YiiComponent::getFinanceManager()
                ->getBalance(self::getUserByName($testUser['username']), Transaction::ACCOUNT_MAIN);
            $this->assertTrue(self::getTestUserBalanceByIndex($index)->equalTo($balance));
        }
    }

    /**
     * @throws \Exception
     */
    public function testMakeRefill(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $currency = $testUser['currency'];
            $user = self::getUserByName($testUser['username']);
            $amount = Money::createFromString(sprintf('%s %s', $currency, random_int(1000, 9999)));

            $testBalance = self::getTestUserBalanceByIndex($index);
            $testBalance = $testBalance->add($amount);

            $refill = YiiComponent::getFinanceManager()->createRefill($user, $amount, $this->getDefaultProvider());
            YiiComponent::getFinanceManager()->makeRefill($refill);

            $balance = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testBalance->equalTo($balance));
        }
    }

    /**
     * @throws \Exception
     */
    public function testRollbackRefill(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $currency = $testUser['currency'];
            $user = self::getUserByName($testUser['username']);
            $amount = Money::createFromString(sprintf('%s %s', $currency, random_int(1000, 9999)));

            $refill = YiiComponent::getFinanceManager()->createRefill($user, $amount, $this->getDefaultProvider());
            YiiComponent::getFinanceManager()->makeRefill($refill);
            YiiComponent::getFinanceManager()->rollbackRefill($refill);

            $balance = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue(self::getTestUserBalanceByIndex($index)->equalTo($balance));
        }
    }


    /**
     * @throws \Exception
     */
    public function testRepeatedRefill(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $currency = $testUser['currency'];
            $user = self::getUserByName($testUser['username']);
            $amount = Money::createFromString(sprintf('%s %s', $currency, random_int(1000, 9999)));

            $testBeginBalance = self::getTestUserBalanceByIndex($index);
            $testRefillBalance = $testBeginBalance->add($amount);

            $refill = YiiComponent::getFinanceManager()->createRefill($user, $amount, $this->getDefaultProvider());
            YiiComponent::getFinanceManager()->makeRefill($refill);
            YiiComponent::getFinanceManager()->makeRefill($refill);
            YiiComponent::getFinanceManager()->makeRefill($refill);

            $balance = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testRefillBalance->equalTo($balance));


            YiiComponent::getFinanceManager()->rollbackRefill($refill);
            YiiComponent::getFinanceManager()->rollbackRefill($refill);
            YiiComponent::getFinanceManager()->rollbackRefill($refill);

            $balance = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testBeginBalance->equalTo($balance));


            YiiComponent::getFinanceManager()->makeRefill($refill);
            YiiComponent::getFinanceManager()->makeRefill($refill);
            YiiComponent::getFinanceManager()->makeRefill($refill);

            $balance = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testRefillBalance->equalTo($balance));
        }
    }

    /**
     * @throws \Exception
     */
    public function testMakePayout(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $user = self::getUserByName($testUser['username']);

            $amount = self::getTestUserBalanceByIndex($index)->divide('2');
            $testBalance = self::getTestUserBalanceByIndex($index)->subtract($amount);

            $payout = YiiComponent::getFinanceManager()
                ->createPayout($user, $amount, $this->getDefaultProvider());
            YiiComponent::getFinanceManager()->makePayout($payout);
            YiiComponent::getFinanceManager()->confirmPayout($payout);

            $balanceMain = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testBalance->equalTo($balanceMain));
        }
    }

    /**
     * @throws \Exception
     */
    public function testRollbackPayout(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $user = self::getUserByName($testUser['username']);

            $testBalance = self::getTestUserBalanceByIndex($index);
            $amount = $testBalance->divide('2');

            $payout = YiiComponent::getFinanceManager()
                ->createPayout($user, $amount, $this->getDefaultProvider());
            YiiComponent::getFinanceManager()->makePayout($payout);
            YiiComponent::getFinanceManager()->rollbackPayout($payout);

            $balanceMain = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testBalance->equalTo($balanceMain));
        }
    }

    /**
     * @throws \Exception
     */
    public function testRepeatedPayout(): void
    {
        $this->randomBalance();

        foreach (self::$testUsers as $index => $testUser) {
            $user = self::getUserByName($testUser['username']);

            $amount = self::getTestUserBalanceByIndex($index)->divide('2');
            $testBeginBalance = self::getTestUserBalanceByIndex($index);
            $testPayoutBalance = $testBeginBalance->subtract($amount);

            $payout = YiiComponent::getFinanceManager()
                ->createPayout($user, $amount, $this->getDefaultProvider());

            YiiComponent::getFinanceManager()->makePayout($payout);
            YiiComponent::getFinanceManager()->makePayout($payout);
            YiiComponent::getFinanceManager()->makePayout($payout);

            $balanceFrozen = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_FROZEN);
            $this->assertTrue($amount->equalTo($balanceFrozen));


            YiiComponent::getFinanceManager()->rollbackPayout($payout);
            YiiComponent::getFinanceManager()->rollbackPayout($payout);
            YiiComponent::getFinanceManager()->rollbackPayout($payout);

            $balanceMain = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testBeginBalance->equalTo($balanceMain));

            YiiComponent::getFinanceManager()->makePayout($payout);

            YiiComponent::getFinanceManager()->confirmPayout($payout);
            YiiComponent::getFinanceManager()->confirmPayout($payout);
            YiiComponent::getFinanceManager()->confirmPayout($payout);

            $balanceMain = YiiComponent::getFinanceManager()->getBalance($user, Transaction::ACCOUNT_MAIN);
            $this->assertTrue($testPayoutBalance->equalTo($balanceMain));
        }
    }

    /**
     * @throws \Exception
     */
    public function testMakeTransferToUser(): void
    {
        $this->randomBalance();

        $fromUser = null;
        $toUser = null;

        foreach (self::$testUsers as $index => $testUser) {
            $fromUser = self::getUserByName($testUser['username']);
            $amount = self::getTestUserBalanceByIndex($index)->divide('2');

            if ($toUser !== null) {
                $beforeBalanceFromUser = YiiComponent::getFinanceManager()
                    ->getBalance($fromUser, Transaction::ACCOUNT_MAIN);
                $beforeBalanceToUser = YiiComponent::getFinanceManager()
                    ->getBalance($toUser, Transaction::ACCOUNT_MAIN);

                YiiComponent::getFinanceManager()->makeTransferToUser($fromUser, $toUser, $amount);

                $afterBalanceFromUser = YiiComponent::getFinanceManager()
                    ->getBalance($fromUser, Transaction::ACCOUNT_MAIN);
                $afterBalanceToUser = YiiComponent::getFinanceManager()
                    ->getBalance($toUser, Transaction::ACCOUNT_MAIN);

                $amountFromUser = YiiComponent::getCurrencyConverter()
                    ->convert($amount, $beforeBalanceFromUser->getCurrency());
                $mustBalanceFromUser = $beforeBalanceFromUser->subtract($amountFromUser);

                $amountToUser = YiiComponent::getCurrencyConverter()
                    ->convert($amount, $beforeBalanceToUser->getCurrency());
                $mustBalanceToUser = $beforeBalanceToUser->add($amountToUser);

                $this->assertTrue($mustBalanceFromUser->equalTo($afterBalanceFromUser));
                $this->assertTrue($mustBalanceToUser->equalTo($afterBalanceToUser));
            }

            $toUser = $fromUser;
        }
    }

    /**
     * @throws \Exception
     */
    private function randomBalance(): void
    {
        for ($i = 0; $i < 10; $i++) {
            $index = random_int(1, 10);
            $sum = random_int(1000, 9999);
            self::$testUsers[$index]['amount'] += $sum;
            $amount = Money::createFromString(sprintf('%s %s', self::$testUsers[$index]['currency'], $sum));

            YiiComponent::getFinanceManager()
                ->makeCorrection(self::getUserByName(self::$testUsers[$index]['username']), $amount);
        }
    }

    /**
     * @return AbstractProvider
     */
    private function getDefaultProvider(): AbstractProvider
    {
        return (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId(uniqid('test', true));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.2018
 */

namespace app\components\rates;

use app\entity\Currency;
use app\exception\finance\CurrencyRateNotFoundException;

abstract class AbstractCurrencyRatesProvider implements CurrencyRateProviderInterface
{
    /**
     * @var string[][]|null
     */
    protected $rates;

    /**
     * @param Currency $currencyFrom
     * @param Currency $currencyTo
     *
     * @return string
     * @throws CurrencyRateNotFoundException
     */
    public function getCurrencyRate(Currency $currencyFrom, Currency $currencyTo): string
    {
        $this->getCurrencyRatesSnapshot();

        if ($currencyFrom->sameAs($currencyTo)) {
            return '1.00000';
        }

        if (!isset($this->rates[$currencyFrom->getCode()][$currencyTo->getCode()])) {
            throw CurrencyRateNotFoundException::forCurrencies($currencyFrom->getCode(), $currencyTo->getCode());
        }

        return $this->rates[$currencyFrom->getCode()][$currencyTo->getCode()];
    }

    /**
     * @return array
     */
    abstract public function getCurrencyRatesSnapshot(): array;
}

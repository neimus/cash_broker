<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\components;

use app\components\rates\CurrencyRateProviderInterface;
use app\entity\Currency;
use app\entity\Money;
use app\exception\finance\CurrencyRateNotFoundException;
use yii\base\Component;
use yii\base\InvalidArgumentException;

class CurrencyConverter extends Component
{
    /**
     * @var CurrencyRateProviderInterface
     */
    private $currencyRateProvider;

    /**
     * @var string
     */
    public $classCurrencyRateProvider;

    /**
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if ($this->classCurrencyRateProvider === null) {
            throw new InvalidArgumentException('Отсутствует обязательный параметр [classCurrencyRateProvider]');
        }
        $currencyRateProvider = \Yii::createObject($this->classCurrencyRateProvider);
        if ($currencyRateProvider instanceof CurrencyRateProviderInterface) {
            $this->currencyRateProvider = $currencyRateProvider;
        } else {
            throw new InvalidArgumentException('Некорректный класс [classCurrencyRateProvider]');
        }
    }

    /**
     * @param Money $money
     * @param Currency $targetCurrency
     *
     * @return \app\entity\Money
     * @throws \InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     */
    public function convert(Money $money, Currency $targetCurrency): Money
    {
        if ($money->getCurrency()->sameAs($targetCurrency)) {
            return $money;
        }
        $currencyRate = $this->currencyRateProvider->getCurrencyRate($money->getCurrency(), $targetCurrency);

        return Money::create($targetCurrency, bcmul($money->getValue(), $currencyRate, Money::SCALE));
    }
}

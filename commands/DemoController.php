<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\commands;

use yii\console\ExitCode;

class DemoController extends ConsoleBase
{

    /**
     *
     * @return int
     * @throws \ReflectionException
     */
    public function actionIndex(): int
    {
        // YiiComponent::getQueueJob()
        //     ->push(RefillJob::package(
        //         [
        //             'toUserId'          => 1,
        //             'amount'            => 'RUB 55.58',
        //             'paymentCode'       => PaymentProviderCodeInterface::PROVIDER_CODE_WINPAY_TYPE_TERMINAL_EUROSET,
        //             'rawData'           => 'card: 4279 6548 5824 578',
        //             'externalPaymentId' => uniqid('refill', true),
        //             'status'            => Refill::STATUS_NEW,
        //         ]));
        // YiiComponent::getQueueJob()
        //     ->push(RefillJob::package(
        //         [
        //             'refillId'      => 11,
        //             'status'            => Refill::STATUS_APPROVED,
        //         ]));

        return ExitCode::OK;
    }

}
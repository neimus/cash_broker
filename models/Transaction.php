<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\models;

use app\entity\Currency;
use app\entity\Money;
use app\models\query\TransactionQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $user_id
 * @property int $initiator_id
 * @property string $type
 * @property int $refill_id
 * @property int $payout_id
 * @property double $account
 * @property string $comment
 * @property int $currency
 * @property int $amount
 * @property string $created_at
 */
class Transaction extends AbstractModel
{
    const COL_ID           = 'id';
    const COL_USER_ID      = 'user_id';
    const COL_INITIATOR_ID = 'initiator_id';
    const COL_TYPE         = 'type';
    const COL_REFILL_ID    = 'refill_id';
    const COL_PAYOUT_ID    = 'payout_id';
    const COL_ACCOUNT      = 'account';
    const COL_COMMENT      = 'comment';
    const COL_CURRENCY     = 'currency';
    const COL_AMOUNT       = 'amount';
    const COL_CREATED_AT   = 'created_at';

    const ACCOUNT_CHARGING = 0;
    const ACCOUNT_MAIN     = 1;
    const ACCOUNT_BONUS    = 2;
    const ACCOUNT_CHECKING = 3;
    const ACCOUNT_PAYOUT   = 4;
    const ACCOUNT_FROZEN   = 5;

    const TYPE_REFILL           = 'refill'; //Пополнение счета клиента
    const TYPE_REFILL_ROLLBACK  = 'refill_rollback'; //Отмена пополнения со счета клиента
    const TYPE_PAYOUT           = 'payout'; //Вывод средств
    const TYPE_PAYOUT_ROLLBACK  = 'payout_rollback'; //Возврат выплаты на счет клиента
    const TYPE_CORRECTION       = 'correction'; //Коррекция
    const TYPE_WRITE_OFF        = 'write_off'; //Списание
    const TYPE_BONUS_ACTIVATION = 'bonus_activation'; //Активация бонуса
    const TYPE_BONUS_REVOCATION = 'bonus_revocation'; //Анулирование бонуса
    const TYPE_TRANSFER_USER    = 'transfer_user'; //Перевод денег в счет другого пользователя
    const TYPE_LOCKOUT          = 'lockout'; //Временно блокирование денег пользователя до разбирательства
    const TYPE_REVOKE_LOCKOUT   = 'revoke_lockout'; //Отзыв блокировки денег

    const TRANSACTION_TYPE = [
        self::TYPE_REFILL,
        self::TYPE_REFILL_ROLLBACK,
        self::TYPE_PAYOUT,
        self::TYPE_PAYOUT_ROLLBACK,
        self::TYPE_CORRECTION,
        self::TYPE_WRITE_OFF,
        self::TYPE_BONUS_ACTIVATION,
        self::TYPE_BONUS_REVOCATION,
        self::TYPE_TRANSFER_USER,
        self::TYPE_LOCKOUT,
        self::TYPE_REVOKE_LOCKOUT,
    ];

    private $user;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    self::COL_USER_ID,
                    self::COL_INITIATOR_ID,
                    self::COL_REFILL_ID,
                    self::COL_PAYOUT_ID,
                    self::COL_ACCOUNT,
                    self::COL_CURRENCY,
                ],
                'integer',
            ],
            [
                self::COL_AMOUNT,
                'double',
                'max' => max(\Yii::$app->params['max_refill_amount_limit'],
                    \Yii::$app->params['max_payout_amount_limit']),
            ],
            [self::COL_TYPE, 'string',],
            [self::COL_COMMENT, 'string', 'max' => 255],
            [self::COL_TYPE, 'in', 'range' => self::TRANSACTION_TYPE, 'strict' => true],
            [
                self::COL_USER_ID,
                'exist',
                'targetClass'     => User::class,
                'skipOnError'     => false,
                'targetAttribute' => [self::COL_USER_ID => User::COL_ID],
            ],
            [
                self::COL_INITIATOR_ID,
                'exist',
                'targetClass'     => User::class,
                'skipOnError'     => true,
                'targetAttribute' => [self::COL_INITIATOR_ID => User::COL_ID],
            ],
            [
                self::COL_REFILL_ID,
                'exist',
                'targetClass'     => Refill::class,
                'skipOnError'     => false,
                'targetAttribute' => [self::COL_REFILL_ID => Refill::COL_ID],
            ],
            [
                self::COL_PAYOUT_ID,
                'exist',
                'targetClass'     => Payout::class,
                'skipOnError'     => false,
                'targetAttribute' => [self::COL_PAYOUT_ID => Payout::COL_ID],
            ],

            [
                [
                    self::COL_USER_ID,
                    self::COL_ACCOUNT,
                    self::COL_CURRENCY,
                    self::COL_AMOUNT,
                    self::COL_TYPE,
                ],
                'required',
            ],
            [[self::COL_CREATED_AT], 'safe',],
        ];
    }

    public function behaviors(): array
    {
        return
            [
                [
                    'class'              => TimestampBehavior::class,
                    'updatedAtAttribute' => false,
                    'value'              => function () {
                        return date('Y-m-d H:i:s', time());
                    },
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            self::COL_ID           => 'ID',
            self::COL_USER_ID      => 'Пользователь',
            self::COL_INITIATOR_ID => 'Инициатор (пользователь) при проведении коррекции или списания',
            self::COL_TYPE         => 'Тип транзакции',
            self::COL_REFILL_ID    => 'ID операции пополнения',
            self::COL_PAYOUT_ID    => 'ID операции списания',
            self::COL_ACCOUNT      => 'Счет учавсвующий в транзакции',
            self::COL_COMMENT      => 'Комментарий',
            self::COL_CURRENCY     => 'Валюта',
            self::COL_AMOUNT       => 'Сумма',
            self::COL_CREATED_AT   => 'Дата создания',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints(): array
    {
        return $this->attributeLabels();
    }

    /**
     * @inheritdoc
     * @return TransactionQuery активный запрос используемый AR классом.
     */
    public static function find(): TransactionQuery
    {
        return new TransactionQuery(static::class);
    }

    public function getRefill()
    {
        return $this->hasOne(Refill::class, [Refill::COL_ID => self::COL_REFILL_ID])->one();
    }

    public function setRefill(?Refill $refill = null)
    {
        $this->refill_id = $refill !== null ? $refill->id : null;

        return $this;
    }

    public function getPayout()
    {
        return $this->hasOne(Payout::class, [Payout::COL_ID => self::COL_PAYOUT_ID])->one();
    }

    public function setPayout(?Payout $payout = null)
    {
        $this->payout_id = $payout !== null ? $payout->id : null;

        return $this;
    }

    /**
     * @return ActiveRecord|User|null
     */
    public function getUser(): ?User
    {
        if ($this->user === null) {
            $this->user = $this->hasOne(User::class, [User::COL_ID => self::COL_USER_ID])->one();
        }

        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        $this->user_id = $user->id;

        return $this;
    }

    /**
     * @param User $initiator
     *
     * @return self
     */
    public function setInitiator(User $initiator): self
    {
        $this->initiator_id = $initiator->id;

        return $this;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function setComment($comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function setAccount($account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function getAmountMoney(): Money
    {
        return Money::create(Currency::create($this->currency), (float)$this->amount);
    }

    /**
     * @param Money $money
     *
     * @return self
     */
    public function setAmountMoney(Money $money): self
    {
        $this->currency = $money->getCurrency()->getValue();
        $this->amount = $money->getValue();

        return $this;
    }

    public function __toString()
    {
        return $this->type;
    }

}
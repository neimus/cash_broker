<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\entity;

class Currency implements CurrencyInterface
{
    const DEFAULT_CURRENCY = self::RUB;

    private $currency;

    /**
     * @param int $currency
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(int $currency)
    {
        if (!array_key_exists($currency, self::CURRENCY_CODES)) {
            throw new \InvalidArgumentException("Некорректная валюта $currency");
        }

        $this->currency = $currency;
    }

    /**
     * @param int $currency
     *
     * @return self
     * @throws \InvalidArgumentException
     */
    public static function create(int $currency): self
    {
        return new self($currency);
    }

    /**
     * @param string $code
     *
     * @return Currency
     * @throws \InvalidArgumentException
     */
    public static function createByCode(string $code): Currency
    {
        return new self(self::getCurrencyIdByAlias($code));
    }

    /**
     * @return Currency
     * @throws \InvalidArgumentException
     */
    public static function createDefault(): Currency
    {
        return self::create(self::DEFAULT_CURRENCY);
    }

    public static function getAllowed(): array
    {
        return array_intersect_key(self::CURRENCY_CODES, array_flip([
            self::USD,
            self::EUR,
            self::RUB,
        ]));
    }

    /**
     * @param $currency
     *
     * @return bool
     */
    public static function isAvailableToUser(int $currency): bool
    {
        return \in_array($currency, self::getAllowed(), true);
    }

    /**
     * @param string $code
     *
     * @return false|int|string
     * @throws \InvalidArgumentException
     */
    private static function getCurrencyIdByAlias(string $code)
    {
        if (!$currency = array_search($code, self::CURRENCY_CODES, true)) {
            throw new \InvalidArgumentException("Псевдоним для валюты '$code' не найден");
        }

        return $currency;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->currency;
    }

    /**
     * 3-letter ISO-code
     *
     * @return string
     */
    public function getCode(): string
    {
        return self::CURRENCY_CODES[$this->currency];
    }

    /**
     * @param Currency $other
     *
     * @return bool
     */
    public function equalsTo(Currency $other): bool
    {
        return $this->currency === $other->currency;
    }

    /**
     * @param Currency $other
     *
     * @return bool
     */
    public function sameAs(Currency $other): bool
    {
        return $this->equalsTo($other);
    }

    public function __toString()
    {
        return $this->getCode();
    }
}

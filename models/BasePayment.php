<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\models;

use app\entity\Currency;
use app\entity\Money;
use app\entity\provider\AbstractProvider;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property int $currency
 * @property double $amount
 * @property int $payment_provider
 * @property string $payment_method
 * @property string $external_payment_id
 * @property string $payment_detail_hash
 * @property string $raw_data
 * @property string $created_at
 */
abstract class BasePayment extends AbstractModel implements PaymentMethodInterface
{
    const COL_ID                  = 'id';
    const COL_USER_ID             = 'user_id';
    const COL_STATUS              = 'status';
    const COL_CURRENCY            = 'currency';
    const COL_AMOUNT              = 'amount';
    const COL_PAYMENT_PROVIDER    = 'payment_provider';
    const COL_PAYMENT_METHOD      = 'payment_method';
    const COL_EXTERNAL_PAYMENT_ID = 'external_payment_id';
    const COL_PAYMENT_DETAIL_HASH = 'payment_detail_hash';
    const COL_RAW_DATA            = 'raw_data';
    const COL_CREATED_AT          = 'created_at';
    const COL_UPDATED_AT          = 'updated_at';

    private $user;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    self::COL_USER_ID,
                    self::COL_CURRENCY,
                    self::COL_PAYMENT_PROVIDER,
                ],
                'integer',
            ],
            [
                self::COL_AMOUNT,
                'double',
                'min' => \Yii::$app->params['min_refill_amount_limit'],
                'max' => \Yii::$app->params['max_refill_amount_limit'],
            ],
            [
                [
                    self::COL_RAW_DATA,
                ],
                'string',
            ],
            [self::COL_PAYMENT_DETAIL_HASH, 'string', 'max' => 32],
            [self::COL_EXTERNAL_PAYMENT_ID, 'string', 'max' => 255],
            [self::COL_STATUS, 'in', 'range' => static::getAvailableStatuses(), 'strict' => true],
            [
                self::COL_USER_ID,
                'exist',
                'targetClass'     => User::class,
                'skipOnError'     => false,
                'targetAttribute' => [self::COL_USER_ID => User::COL_ID],
            ],
            [self::COL_PAYMENT_METHOD, 'in', 'range' => self::PAYMENT_METHODS],

            [
                [
                    self::COL_USER_ID,
                    self::COL_STATUS,
                    self::COL_CURRENCY,
                    self::COL_AMOUNT,
                    self::COL_PAYMENT_PROVIDER,
                    self::COL_PAYMENT_METHOD,
                    self::COL_EXTERNAL_PAYMENT_ID,
                ],
                'required',
            ],
            [
                [self::COL_PAYMENT_PROVIDER, self::COL_EXTERNAL_PAYMENT_ID],
                'unique',
                'targetAttribute' => [self::COL_PAYMENT_PROVIDER, self::COL_EXTERNAL_PAYMENT_ID],
            ],
            [[self::COL_CREATED_AT, self::COL_UPDATED_AT], 'safe',],
        ];
    }

    public function behaviors(): array
    {
        return
            [
                [
                    'class' => TimestampBehavior::class,
                    'value' => function () {
                        return date('Y-m-d H:i:s', time());
                    },
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints(): array
    {
        return $this->attributeLabels();
    }

    abstract public static function getAvailableStatuses();

    /**
     * @return ActiveRecord|User
     */
    public function getUser(): User
    {
        if ($this->user === null) {
            $this->user = $this->hasOne(User::class, [User::COL_ID => self::COL_USER_ID])->one();
        }

        return $this->user;
    }

    /**
     * @param \app\models\User $user
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        $this->user_id = $user->id;

        return $this;
    }

    /**
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function getAmountMoney(): Money
    {
        return Money::create(Currency::create($this->currency), (float)$this->amount);
    }

    /**
     * @param Money $money
     *
     * @return self
     */
    public function setAmountMoney(Money $money): self
    {
        $this->currency = $money->getCurrency()->getValue();
        $this->amount = $money->getValue();

        return $this;
    }

    /**
     * @param AbstractProvider $provider
     *
     * @return self
     */
    public function setProvider(AbstractProvider $provider): self
    {
        $this->payment_provider = $provider->getCodeProvider();
        $this->payment_method = $provider->getMethod();
        $this->external_payment_id = $provider->getExternalId();
        $this->raw_data = $provider->getRawData();
        $this->payment_detail_hash = $provider->getDetailHash();

        return $this;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    abstract public function isInTerminalStatus(): bool;

    abstract public function isInNonTerminalStatus(): bool;

    abstract public function isInWaitingStatus(): bool;

    abstract public function allowRollback(): bool;
}
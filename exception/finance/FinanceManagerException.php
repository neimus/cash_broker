<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\exception\finance;

class FinanceManagerException extends \RuntimeException
{
}

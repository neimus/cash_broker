<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\helpers;

class ClassHelper
{
    /**
     * @param string $namespace
     * @param string $shortNameClass
     * @param string $suffixClass
     *
     * @return string
     */
    public static function getNamespaceByShortName(
        string $namespace,
        string $shortNameClass,
        string $suffixClass
    ): string {
        $className = ucwords(preg_replace('/[\W_\.]/', ' ', $shortNameClass));

        return sprintf('%s\%s%s', $namespace, preg_replace('/[\s]/', '', $className), $suffixClass);
    }
}
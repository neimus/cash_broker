<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\event;

use app\entity\FinanceTransfer;
use yii\base\Event;

class FinanceManagerEvent extends Event
{
    /**
     * @var FinanceTransfer|null
     */
    private $financeTransfer;

    private $message = '';

    /**
     * @return FinanceTransfer|null
     */
    public function getFinanceTransfer(): ?FinanceTransfer
    {
        return $this->financeTransfer;
    }

    /**
     * @param FinanceTransfer|null $financeTransfer
     *
     * @return self
     */
    public function setFinanceTransfer(FinanceTransfer $financeTransfer): self
    {
        $this->financeTransfer = $financeTransfer;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\components\rates;

use app\models\CurrencyRate;

class DbCurrencyRatesProvider extends AbstractCurrencyRatesProvider
{
    public function getCurrencyRatesSnapshot(): array
    {
        if ($this->rates === null) {
            $this->rates = CurrencyRate::find()->getAllIndexedByCurrencies();
        }

        return $this->rates;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\models;

use app\models\query\RefillQuery;

/**
 * Модель для таблицы refill
 */
class Refill extends BasePayment
{
    const STATUS_NEW      = 'new';
    const STATUS_WAITING  = 'waiting';
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';

    const REFILL_STATUS = [
        self::STATUS_NEW,
        self::STATUS_WAITING,
        self::STATUS_APPROVED,
        self::STATUS_DECLINED,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'refill';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            self::COL_ID                  => 'ID',
            self::COL_USER_ID             => 'Пользователь',
            self::COL_STATUS              => 'Статус',
            self::COL_CURRENCY            => 'Валюта',
            self::COL_AMOUNT              => 'Сумма',
            self::COL_PAYMENT_PROVIDER    => 'Провайдер, через которого осуществляется платеж',
            self::COL_PAYMENT_METHOD      => 'Метод платежа',
            self::COL_EXTERNAL_PAYMENT_ID => 'Внешний ID платежа полученный от провайдера',
            self::COL_PAYMENT_DETAIL_HASH => 'Сведения о счета в виде hash-а',
            self::COL_RAW_DATA            => 'Дополнительные данные от провайдера',
            self::COL_CREATED_AT          => 'Дата создания',
            self::COL_UPDATED_AT          => 'Дата обновления информации',
        ];
    }

    public static function getAvailableStatuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_WAITING,
            self::STATUS_APPROVED,
            self::STATUS_DECLINED,
        ];
    }

    /**
     * @inheritdoc
     * @return RefillQuery активный запрос используемый AR классом.
     */
    public static function find(): RefillQuery
    {
        return new RefillQuery(static::class);
    }

    public function isInTerminalStatus(): bool
    {
        return $this->status === self::STATUS_APPROVED || $this->status === self::STATUS_DECLINED;
    }

    public function isInNonTerminalStatus(): bool
    {
        return $this->status === self::STATUS_NEW || $this->status === self::STATUS_WAITING;
    }

    public function isInWaitingStatus(): bool
    {
        return $this->isInNonTerminalStatus();
    }

    public function allowRollback(): bool
    {
        return $this->status === self::STATUS_APPROVED;
    }

    public function __toString()
    {
        try {
            return sprintf('Пополнение на счет %d в сумме %s', $this->user_id, (string)$this->getAmountMoney());
        } catch (\InvalidArgumentException $exception) {
            return $exception->getMessage();
        }
    }
}
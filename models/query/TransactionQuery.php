<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.2018 */

namespace app\models\query;

use app\entity\Money;
use app\models\Transaction;
use app\models\User;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * ActiveQuery для [[Transaction]].
 *
 * @see Transaction
 */
class TransactionQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Transaction[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Transaction|null
     */
    public function one($db = null): ?Transaction
    {
        return parent::one($db);
    }

    /**
     * @param User $user
     * @param int $account
     * @param \DateTime $upToDate
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function getBalance(User $user, int $account, ?\DateTime $upToDate = null): Money
    {
        $this->select(new Expression(sprintf('SUM(%s)', Transaction::COL_AMOUNT)))
            ->where(['=', Transaction::COL_USER_ID, $user->id])
            ->andWhere(['=', Transaction::COL_ACCOUNT, $account])
            ->andWhere(['=', Transaction::COL_CURRENCY, $user->getCurrency()->getValue()]);
        if ($upToDate !== null) {
            $this->andWhere(['<=', Transaction::COL_CREATED_AT, $upToDate]);
        }

        return new Money($user->getCurrency(), $this->scalar());
    }

    /**
     * @param User $user
     * @param int $account
     * @param int $version
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function getBalanceAtVersion(User $user, int $account, int $version): Money
    {
        $this->select(new Expression(sprintf('SUM(%s)', Transaction::COL_AMOUNT)))
            ->where(['=', Transaction::COL_USER_ID, $user->id])
            ->andWhere(['=', Transaction::COL_ACCOUNT, $account])
            ->andWhere(['=', Transaction::COL_CURRENCY, $user->getCurrency()->getValue()])
            ->andWhere(['=', Transaction::COL_ID, $version]);

        return new Money($user->getCurrency(), $this->scalar());
    }

    /**
     * @param User $user
     * @param int $account
     * @param int $payoutId
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function getBalanceByPayoutId(User $user, int $account, int $payoutId): Money
    {
        $this->select(new Expression(sprintf('SUM(%s)', Transaction::COL_AMOUNT)))
            ->where(['=', Transaction::COL_USER_ID, $user->id])
            ->andWhere(['=', Transaction::COL_ACCOUNT, $account])
            ->andWhere(['=', Transaction::COL_CURRENCY, $user->getCurrency()->getValue()])
            ->andWhere(['=', Transaction::COL_PAYOUT_ID, $payoutId]);

        return new Money($user->getCurrency(), $this->scalar());
    }

    /**
     * @param User $user
     * @param int $account
     * @param int $refillId
     *
     * @return Money
     *
     * @throws \InvalidArgumentException
     */
    public function getBalanceByRefillId(User $user, int $account, int $refillId): Money
    {
        $this->select(new Expression(sprintf('SUM(%s)', Transaction::COL_AMOUNT)))
            ->where(['=', Transaction::COL_USER_ID, $user->id])
            ->andWhere(['=', Transaction::COL_ACCOUNT, $account])
            ->andWhere(['=', Transaction::COL_CURRENCY, $user->getCurrency()->getValue()])
            ->andWhere(['=', Transaction::COL_REFILL_ID, $refillId]);

        return new Money($user->getCurrency(), $this->scalar());
    }

    /**
     * Возвращает баланс пользователя с блокировкой записи (!!!только для MYSQL!!!)
     *
     * @param User $user
     * @param int $account
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     * @throws \yii\db\Exception
     */
    public function getBalanceWithLock(User $user, int $account): string
    {
        $sql = <<<SQL
                SELECT SUM(`amount`) 
                FROM `transaction` 
                WHERE `user_id` = :user_id AND `account` = :account AND `currency` = :currency
                LOCK IN SHARE MODE
SQL;

        $amount = \Yii::$app->getDb()->createCommand($sql, [
            ':user_id'  => $user->id,
            ':account'  => $account,
            ':currency' => $user->getCurrency()->getValue(),
        ])->queryScalar();

        return new Money($user->getCurrency(), $amount);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\exception\console;

use yii\console\Exception;

class FileNotFoundException extends Exception
{

}
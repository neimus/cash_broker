<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.2018 */

namespace app\models\query;

use app\models\Refill;
use yii\db\ActiveQuery;

/**
 * ActiveQuery для [[Refill]].
 *
 * @see Refill
 */
class RefillQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Refill[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Refill|null
     */
    public function one($db = null): ?Refill
    {
        return parent::one($db);
    }
}

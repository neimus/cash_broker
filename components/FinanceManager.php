<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\components;

use app\entity\FinanceTransfer;
use app\entity\Money;
use app\entity\provider\AbstractProvider;
use app\entity\provider\OtherProvider;
use app\event\FinanceManagerEvent;
use app\exception\finance\CurrencyRateNotFoundException;
use app\exception\finance\FinanceManagerException;
use app\exception\finance\FinanceTransferInvalidArgument;
use app\exception\finance\InsufficientFundsException;
use app\exception\finance\TransactionException;
use app\helpers\YiiComponent;
use app\models\BasePayment;
use app\models\Payout;
use app\models\Refill;
use app\models\Transaction;
use app\models\User;
use yii\base\Component;

class FinanceManager extends Component
{
    const EVENT_BEFORE_TRANSACTION  = 'beforeFinanceTransaction';
    const EVENT_AFTER_TRANSACTION   = 'afterFinanceTransaction';
    const EVENT_RETRY_TRANSACTION   = 'retryFinanceTransaction';
    const EVENT_SUCCESS_TRANSACTION = 'successFinanceTransaction';
    const EVENT_ERROR_TRANSACTION   = 'errorFinanceTransaction';
    const EVENT_SUCCESS_PAYOUT      = 'successPayout';
    const EVENT_ROLLBACK_PAYOUT     = 'rollbackPayout';
    const EVENT_SUCCESS_REFILL      = 'successRefill';
    const EVENT_ROLLBACK_REFILL     = 'rollbackRefill';

    /**
     * Максимальное время блокировки транзакции (секунды)
     */
    const MUTEX_LOCK_TIMEOUT = 20;

    /**
     * @param User $user
     * @param int $account
     *
     * @param \DateTime|null $upToDate
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function getBalance(User $user, int $account, ?\DateTime $upToDate = null): Money
    {
        return Transaction::find()->getBalance($user, $account, $upToDate);
    }

    /**
     * @param User $fromUser
     * @param User $toUser
     * @param Money $amount
     * @param string $comment
     *
     * @throws FinanceManagerException
     * @throws \yii\base\InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function makeTransferToUser(User $fromUser, User $toUser, Money $amount, string $comment = ''): void
    {
        $financeTransfer = (new FinanceTransfer())
            ->setAmount($amount)
            ->setComment($comment);

        $provider = (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId(uniqid('transfer', true));

        $payout = $this->createPayout($fromUser, $amount, $provider, Payout::STATUS_COMPLETED);
        $refill = $this->createRefill($toUser, $amount, $provider, Refill::STATUS_APPROVED);

        $financeTransfer
            ->setPayout($payout)
            ->setRefill($refill)
            ->setFromUser($fromUser)
            ->setToUser($toUser)
            ->setFromAccount(Transaction::ACCOUNT_MAIN)
            ->setToAccount(Transaction::ACCOUNT_MAIN)
            ->setTransactionType(Transaction::TYPE_TRANSFER_USER);

        $this->makeFinanceTransfer($financeTransfer);
    }

    /**
     * @param Refill $refill
     * @param string $comment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function makeRefill(Refill $refill, string $comment = ''): void
    {
        $amountRefill = $this->getBalanceByRefill($refill, Transaction::ACCOUNT_MAIN);

        $financeTransfer = (new FinanceTransfer())
            ->setRefill($refill)
            ->setFromUser($refill->getUser())
            ->setToUser($refill->getUser())
            ->setAmount($refill->getAmountMoney())
            ->setFromAccount(Transaction::ACCOUNT_CHARGING)
            ->setToAccount(Transaction::ACCOUNT_MAIN)
            ->setComment($comment)
            ->setTransactionType(Transaction::TYPE_REFILL);

        if ($amountRefill->equalToZero()) {
            $this->makeFinanceTransfer($financeTransfer);
        } else {
            $this->createErrorEvent($financeTransfer, 'Зачисление уже сделано');
        }
    }

    /**
     * @param Refill $refill
     * @param string $comment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function rollbackRefill(Refill $refill, string $comment = ''): void
    {
        $amountRefill = $this->getBalanceByRefill($refill, Transaction::ACCOUNT_MAIN);

        $financeTransfer = (new FinanceTransfer())
            ->setRefill($refill)
            ->setFromUser($refill->getUser())
            ->setToUser($refill->getUser())
            ->setAmount($refill->getAmountMoney())
            ->setFromAccount(Transaction::ACCOUNT_MAIN)
            ->setToAccount(Transaction::ACCOUNT_CHARGING)
            ->setComment($comment)
            ->setTransactionType(Transaction::TYPE_REFILL_ROLLBACK);

        if ($amountRefill->equalTo($refill->getAmountMoney())) {
            $this->on(self::EVENT_SUCCESS_TRANSACTION, function () {
                $this->trigger(self::EVENT_ROLLBACK_REFILL);
            });

            $this->makeFinanceTransfer($financeTransfer);
        } else {
            $this->createErrorEvent($financeTransfer, 'Возврат зачисления уже сделано');
        }
    }

    /**
     * Создает транзакцию на выплату (сумма замораживается на другом счете)
     *
     * @param Payout $payout
     * @param string $comment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function makePayout(Payout $payout, string $comment = ''): void
    {
        $amountPayout = $this->getBalanceByPayout($payout, Transaction::ACCOUNT_FROZEN);

        $financeTransfer = (new FinanceTransfer())
            ->setPayout($payout)
            ->setFromUser($payout->getUser())
            ->setToUser($payout->getUser())
            ->setAmount($payout->getAmountMoney())
            ->setFromAccount(Transaction::ACCOUNT_MAIN)
            ->setToAccount(Transaction::ACCOUNT_FROZEN)
            ->setComment($comment)
            ->setTransactionType(Transaction::TYPE_PAYOUT);

        if ($amountPayout->equalToZero()) {
            $this->makeFinanceTransfer($financeTransfer);
        } else {
            $this->createErrorEvent($financeTransfer, 'Резервирование средств для выплаты уже сделано');
        }
    }

    /**
     * Создает транзакцию на откат выплаты (сумма возвращается обратно на счет)
     *
     * @param Payout $payout
     * @param string $comment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function rollbackPayout(Payout $payout, string $comment = ''): void
    {
        $amountPayout = $this->getBalanceByPayout($payout, Transaction::ACCOUNT_FROZEN);

        $financeTransfer = (new FinanceTransfer())
            ->setPayout($payout)
            ->setFromUser($payout->getUser())
            ->setToUser($payout->getUser())
            ->setAmount($payout->getAmountMoney())
            ->setFromAccount(Transaction::ACCOUNT_FROZEN)
            ->setToAccount(Transaction::ACCOUNT_MAIN)
            ->setComment($comment)
            ->setTransactionType(Transaction::TYPE_PAYOUT_ROLLBACK);

        if ($amountPayout->equalTo($payout->getAmountMoney())) {
            $this->on(self::EVENT_SUCCESS_TRANSACTION, function () {
                $this->trigger(self::EVENT_ROLLBACK_PAYOUT);
            });

            $this->makeFinanceTransfer($financeTransfer);
        } else {
            $this->createErrorEvent($financeTransfer, 'Откат зарезервированных средств для выплаты уже сделано');
        }
    }

    /**
     * Создает транзакцию на подтверждение выплаты
     *
     * @param Payout $payout
     * @param string $comment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function confirmPayout(Payout $payout, string $comment = ''): void
    {
        $amountPayout = $this->getBalanceByPayout($payout, Transaction::ACCOUNT_FROZEN);

        $financeTransfer = (new FinanceTransfer())
            ->setPayout($payout)
            ->setFromUser($payout->getUser())
            ->setToUser($payout->getUser())
            ->setAmount($payout->getAmountMoney())
            ->setFromAccount(Transaction::ACCOUNT_FROZEN)
            ->setToAccount(Transaction::ACCOUNT_PAYOUT)
            ->setComment($comment)
            ->setTransactionType(Transaction::TYPE_PAYOUT);

        if ($amountPayout->equalTo($payout->getAmountMoney())) {
            $this->on(self::EVENT_SUCCESS_TRANSACTION, function () {
                $this->trigger(self::EVENT_SUCCESS_PAYOUT);
            });

            $this->makeFinanceTransfer($financeTransfer);
        } else {
            $this->createErrorEvent($financeTransfer, 'Подтверждение выплаты уже сделано');
        }
    }

    /**
     * @param User $user
     * @param Money $amount
     * @param string $comment
     *
     * @throws FinanceManagerException
     * @throws CurrencyRateNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function makeLockout(User $user, Money $amount, string $comment = ''): void
    {
        $financeTransfer = (new FinanceTransfer())
            ->setAmount($amount)
            ->setComment($comment);

        $provider = (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId(uniqid('lockout', true));

        $payout = $this->createPayout($user, $amount, $provider, Payout::STATUS_COMPLETED);
        $financeTransfer
            ->setPayout($payout)
            ->setFromUser($payout->getUser())
            ->setToUser($payout->getUser())
            ->setFromAccount(Transaction::ACCOUNT_MAIN)
            ->setToAccount(Transaction::ACCOUNT_CHECKING)
            ->setTransactionType(Transaction::TYPE_LOCKOUT);

        $this->makeFinanceTransfer($financeTransfer);
    }

    /**
     * @param Payout $payout
     * @param string $comment
     *
     * @throws FinanceManagerException
     * @throws CurrencyRateNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function revokeLockout(Payout $payout, string $comment = ''): void
    {
        $financeTransfer = (new FinanceTransfer())
            ->setAmount($payout->getAmountMoney())
            ->setComment($comment);

        $provider = (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId($payout->external_payment_id);

        $refill = $this->createRefill($payout->getUser(), $payout->getAmountMoney(), $provider,
            Refill::STATUS_APPROVED);
        $financeTransfer
            ->setRefill($refill)
            ->setFromUser($refill->getUser())
            ->setToUser($refill->getUser())
            ->setFromAccount(Transaction::ACCOUNT_CHECKING)
            ->setToAccount(Transaction::ACCOUNT_MAIN)
            ->setTransactionType(Transaction::TYPE_REVOKE_LOCKOUT);

        $this->makeFinanceTransfer($financeTransfer);
    }

    /**
     * @param User $user
     * @param Money $amount
     * @param string $comment
     *
     * @throws FinanceManagerException
     * @throws CurrencyRateNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function makeCorrection(User $user, Money $amount, string $comment = ''): void
    {
        $financeTransfer = (new FinanceTransfer())
            ->setAmount($amount)
            ->setComment($comment);

        $provider = (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId(uniqid('correction', true));

        if ($amount->greaterThanOrEqualZero()) {
            $refill = $this->createRefill($user, $amount, $provider, Refill::STATUS_APPROVED);
            $financeTransfer
                ->setRefill($refill)
                ->setFromUser($refill->getUser())
                ->setToUser($refill->getUser())
                ->setFromAccount(Transaction::ACCOUNT_CHARGING)
                ->setToAccount(Transaction::ACCOUNT_MAIN)
                ->setTransactionType(Transaction::TYPE_CORRECTION);
        } else {
            $payout = $this->createPayout($user, $amount, $provider, Payout::STATUS_COMPLETED);
            $financeTransfer
                ->setPayout($payout)
                ->setFromUser($payout->getUser())
                ->setToUser($payout->getUser())
                ->setFromAccount(Transaction::ACCOUNT_MAIN)
                ->setToAccount(Transaction::ACCOUNT_CHARGING)
                ->setTransactionType(Transaction::TYPE_WRITE_OFF);
        }

        $this->makeFinanceTransfer($financeTransfer);
    }

    /**
     * @param User $user
     * @param Money $amount
     * @param string $comment
     *
     * @throws FinanceManagerException
     * @throws CurrencyRateNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function makeBonus(User $user, Money $amount, string $comment = ''): void
    {
        $financeTransfer = (new FinanceTransfer())
            ->setAmount($amount)
            ->setComment($comment);

        $provider = (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId(uniqid('bonus', true));

        $refill = $this->createRefill($user, $amount, $provider, Refill::STATUS_APPROVED);
        $financeTransfer
            ->setRefill($refill)
            ->setFromUser($refill->getUser())
            ->setToUser($refill->getUser())
            ->setFromAccount(Transaction::ACCOUNT_CHARGING)
            ->setToAccount(Transaction::ACCOUNT_BONUS)
            ->setTransactionType(Transaction::TYPE_BONUS_ACTIVATION);

        $this->makeFinanceTransfer($financeTransfer);
    }

    /**
     * @param User $user
     * @param Money $amount
     * @param string $comment
     *
     * @throws CurrencyRateNotFoundException
     * @throws FinanceManagerException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function revokeBonus(User $user, Money $amount, string $comment = ''): void
    {
        $financeTransfer = (new FinanceTransfer())
            ->setAmount($amount)
            ->setComment($comment);

        $provider = (new OtherProvider())
            ->setMethod(Refill::METHOD_OTHER)
            ->setExternalId(uniqid('bonus', true));

        $payout = $this->createPayout($user, $amount, $provider, Payout::STATUS_COMPLETED);
        $financeTransfer
            ->setPayout($payout)
            ->setFromUser($payout->getUser())
            ->setToUser($payout->getUser())
            ->setFromAccount(Transaction::ACCOUNT_BONUS)
            ->setToAccount(Transaction::ACCOUNT_CHARGING)
            ->setTransactionType(Transaction::TYPE_BONUS_REVOCATION);

        $this->makeFinanceTransfer($financeTransfer);
    }

    /**
     * @param User $user
     * @param Money $amount
     * @param AbstractProvider $provider
     * @param string $status
     *
     * @return Refill|BasePayment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     */
    public function createRefill(
        User $user,
        Money $amount,
        AbstractProvider $provider,
        string $status = Refill::STATUS_NEW
    ): Refill {
        $refill = (new Refill())
            ->setUser($user)
            ->setAmountMoney($amount)
            ->setProvider($provider)
            ->setStatus($status);
        if (!$refill->validate() || !$refill->save()) {
            throw new FinanceManagerException($refill->getModelErrors());
        }

        return $refill;
    }

    /**
     * @param User $user
     * @param Money $amount
     * @param AbstractProvider $provider
     * @param string $status
     *
     * @return Payout|BasePayment
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws FinanceManagerException
     */
    public function createPayout(
        User $user,
        Money $amount,
        AbstractProvider $provider,
        string $status = Payout::STATUS_NEW
    ): Payout {
        $payout = (new Payout())
            ->setUser($user)
            ->setAmountMoney($amount)
            ->setProvider($provider)
            ->setStatus($status);
        if (!$payout->validate() || !$payout->save()) {
            throw new FinanceManagerException($payout->getModelErrors());
        }

        return $payout;
    }

    /**
     * @param Payout $payout
     * @param int $account
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    private function getBalanceByPayout(Payout $payout, int $account): Money
    {
        return Transaction::find()->getBalanceByPayoutId($payout->getUser(), $account, $payout->id);
    }

    /**
     * @param Refill $refill
     * @param int $account
     *
     * @return Money
     * @throws \InvalidArgumentException
     */
    private function getBalanceByRefill(Refill $refill, int $account): Money
    {
        return Transaction::find()->getBalanceByRefillId($refill->getUser(), $account, $refill->id);
    }

    /**
     * @param FinanceTransfer $financeTransfer
     *
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws \Exception
     */
    private function makeFinanceTransfer(FinanceTransfer $financeTransfer): void
    {
        $event = (new FinanceManagerEvent)->setFinanceTransfer($financeTransfer);
        $this->trigger(self::EVENT_BEFORE_TRANSACTION, $event);
        try {
            if (!$financeTransfer->isValid()) {
                throw FinanceTransferInvalidArgument::createFromFinanceTransfer($financeTransfer);
            }
            $this->doTransfer($financeTransfer, $event);
            $this->trigger(self::EVENT_SUCCESS_TRANSACTION, $event);
        } catch (\Exception $exception) {
            \Yii::error($exception->getMessage(), 'financeManager');
            $this->trigger(self::EVENT_ERROR_TRANSACTION, $event);
            throw $exception;
        } finally {
            $this->trigger(self::EVENT_AFTER_TRANSACTION, $event);
        }
    }

    /**
     * @param FinanceTransfer $financeTransfer
     * @param FinanceManagerEvent $event
     * @param int $retry
     *
     * @throws InsufficientFundsException Если недостаточно средств
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws FinanceTransferInvalidArgument
     * @throws \yii\db\Exception
     * @throws \InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     * @throws \Exception
     */
    private function doTransfer(FinanceTransfer $financeTransfer, FinanceManagerEvent $event, int $retry = 10): void
    {
        if ($this->mutexAcquire($financeTransfer)) {
            $dbTransaction = \Yii::$app->getDb()->beginTransaction();
            try {
                $this->checkBalance($financeTransfer);

                $creditTransaction = $this->createCreditTransaction($financeTransfer);
                $debitTransaction = $this->createDebitTransaction($financeTransfer);

                if ($creditTransaction->save() && $debitTransaction->save()) {
                    $dbTransaction->commit();
                } else {
                    throw new TransactionException(sprintf('Не удалось выполнить перевод %s', $financeTransfer));
                }

            } catch (\Exception $exception) {
                $dbTransaction->rollBack();

                throw $exception;
            } finally {
                $this->mutexRelease($financeTransfer);
            }
        } elseif ($retry > 0) {
            $this->trigger(self::EVENT_RETRY_TRANSACTION, $event);
            $retry--;
            usleep(300000); // 0.3 задержка
            $this->doTransfer($financeTransfer, $event, $retry);
        } else {
            throw new TransactionException(sprintf('Не удалось выполнить перевод %s', $financeTransfer));
        }
    }

    /**
     * Попытка блокировки счетов
     *
     * @param FinanceTransfer $financeTransfer
     *
     * @return bool
     */
    private function mutexAcquire(FinanceTransfer $financeTransfer): bool
    {
        $lockNameFromUser = $this->getLockNameByFromUser($financeTransfer);
        $lockNameToUser = $this->getLockNameByToUser($financeTransfer);

        if ($lockNameFromUser === $lockNameToUser) {
            return YiiComponent::getMutex()->acquire($lockNameFromUser, self::MUTEX_LOCK_TIMEOUT);
        }

        $lockFrom = YiiComponent::getMutex()->acquire($lockNameFromUser, self::MUTEX_LOCK_TIMEOUT);
        $lockTo = YiiComponent::getMutex()->acquire($lockNameToUser, self::MUTEX_LOCK_TIMEOUT);

        // Если удалось создать блокировку у обоих счетов
        if ($lockFrom && $lockTo) {
            return true;
        }

        // Если во время создания заблокировали не все счета, отпускаем те, которые заблокировали
        if ($lockFrom === true) {
            YiiComponent::getMutex()->release($lockNameFromUser);
        }
        if ($lockTo === true) {
            YiiComponent::getMutex()->release($lockNameToUser);
        }

        return false;
    }

    /**
     * Разблокировка счетов
     *
     * @param $financeTransfer
     */
    private function mutexRelease($financeTransfer): void
    {
        $lockNameFromUser = $this->getLockNameByFromUser($financeTransfer);
        $lockNameToUser = $this->getLockNameByToUser($financeTransfer);
        if ($lockNameFromUser === $lockNameToUser) {
            YiiComponent::getMutex()->release($lockNameFromUser);
        } else {
            YiiComponent::getMutex()->release($lockNameFromUser);
            YiiComponent::getMutex()->release($lockNameToUser);
        }
    }

    private function getLockNameByFromUser(FinanceTransfer $financeTransfer): string
    {
        return 'transaction_id_' . $financeTransfer->getFromUser()->id;
    }

    private function getLockNameByToUser(FinanceTransfer $financeTransfer): string
    {
        return 'transaction_id_' . $financeTransfer->getToUser()->id;
    }

    /**
     * @param FinanceTransfer $financeTransfer
     *
     * @throws InsufficientFundsException
     * @throws \InvalidArgumentException
     * @throws CurrencyRateNotFoundException
     */
    private function checkBalance(FinanceTransfer $financeTransfer): void
    {
        if ($financeTransfer->getFromAccount() !== Transaction::ACCOUNT_CHARGING) {
            $balanceAmount = $this->getBalance($financeTransfer->getFromUser(), $financeTransfer->getFromAccount());

            $transferAmount = YiiComponent::getCurrencyConverter()
                ->convert($financeTransfer->getAmount(), $balanceAmount->getCurrency());

            if ($transferAmount->greaterThan($balanceAmount)) {
                $message = sprintf('Недостаточно средств на счете %d. 
                Для проведения транзакции необходимо %s, фактически %s',
                    $financeTransfer->getFromUser()->id, $financeTransfer->getAmount(), $balanceAmount);

                throw new InsufficientFundsException($message);
            }
        }
    }

    /**
     * @param FinanceTransfer $financeTransfer
     *
     * @return Transaction
     * @throws CurrencyRateNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws \InvalidArgumentException
     */
    private function createCreditTransaction(FinanceTransfer $financeTransfer): Transaction
    {
        $transferAmount = YiiComponent::getCurrencyConverter()
            ->convert($financeTransfer->getAmount(), $financeTransfer->getFromUser()->getCurrency());

        $creditTransaction = (new Transaction())
            ->setUser($financeTransfer->getFromUser())
            ->setAmountMoney($transferAmount->getNegative())
            ->setType($financeTransfer->getTransactionType())
            ->setAccount($financeTransfer->getFromAccount())
            ->setComment($financeTransfer->getComment());

        if ($financeTransfer->hasPayout()) {
            $creditTransaction->setPayout($financeTransfer->getPayout());
        }
        if ($financeTransfer->hasRefill()) {
            $creditTransaction->setRefill($financeTransfer->getRefill());
        }

        if ($financeTransfer->hasInitiator()) {
            $creditTransaction->setInitiator($financeTransfer->getInitiator());
        }

        if (!$creditTransaction->validate()) {
            throw new TransactionException($creditTransaction->getModelErrors());
        }

        return $creditTransaction;
    }

    /**
     * @param FinanceTransfer $financeTransfer
     *
     * @return Transaction
     * @throws CurrencyRateNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws TransactionException
     * @throws \InvalidArgumentException
     */
    private function createDebitTransaction(FinanceTransfer $financeTransfer): Transaction
    {
        $transferAmount = YiiComponent::getCurrencyConverter()
            ->convert($financeTransfer->getAmount(), $financeTransfer->getToUser()->getCurrency());

        $creditTransaction = (new Transaction())
            ->setUser($financeTransfer->getToUser())
            ->setAmountMoney($transferAmount)
            ->setType($financeTransfer->getTransactionType())
            ->setAccount($financeTransfer->getToAccount())
            ->setComment($financeTransfer->getComment());

        if ($financeTransfer->hasPayout()) {
            $creditTransaction->setPayout($financeTransfer->getPayout());
        } elseif ($financeTransfer->hasRefill()) {
            $creditTransaction->setRefill($financeTransfer->getRefill());
        }

        if ($financeTransfer->hasInitiator()) {
            $creditTransaction->setInitiator($financeTransfer->getInitiator());
        }

        if (!$creditTransaction->validate()) {
            throw new TransactionException($creditTransaction->getModelErrors());
        }

        return $creditTransaction;
    }

    /**
     * @param FinanceTransfer $financeTransfer
     * @param string $message
     */
    private function createErrorEvent(FinanceTransfer $financeTransfer, string $message = ''): void
    {
        $event = (new FinanceManagerEvent)
            ->setFinanceTransfer($financeTransfer)
            ->setMessage($message);
        $this->trigger(self::EVENT_ERROR_TRANSACTION, $event);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.2018
 */

namespace app\models\query;

use app\models\CurrencyRate;
use yii\db\ActiveQuery;

/**
 * ActiveQuery для [[CurrencyRate]].
 *
 * @see CurrencyRate
 */
class CurrencyRateQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return CurrencyRate[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CurrencyRate|null
     */
    public function one($db = null): ?CurrencyRate
    {
        return parent::one($db);
    }

    /**
     * @return array Массив валют вида: [{currency_from}][{{currency_to}}] = {ratio}
     *      Например:
     *      [
     *          'RUB' => [
     *              'USD' => {ratio},
     *              'EVR' => {ratio},
     *          ],
     *          'USD' => [
     *              'RUB' => {ratio},
     *              'EVR' => {ratio},
     *          ]
     *          ...
     *      ]
     *
     */
    public function getAllIndexedByCurrencies(): array
    {
        $currencyRates = $this->asArray()->all();
        $result = [];
        foreach ($currencyRates as $item) {
            $result[$item[CurrencyRate::COL_CURRENCY_FROM]][$item[CurrencyRate::COL_CURRENCY_TO]] = $item[CurrencyRate::COL_RATIO];
        }

        return $result;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\entity\provider;

interface PaymentProviderInterface
{
    const PAYMENT_PROVIDER_OTHER     = 0;
    const PAYMENT_PROVIDER_ECOMMPAY  = 100;
    const PAYMENT_PROVIDER_WINPAY    = 200;
    const PAYMENT_PROVIDER_PAYANYWAY = 300;
    const PAYMENT_PROVIDER_FATPAY    = 400;
    const PAYMENT_PROVIDER_DUSUPAY   = 500;
    const PAYMENT_PROVIDER_SPEEDPAY  = 600;

    const PAYMENT_PROVIDERS = [
        self::PAYMENT_PROVIDER_OTHER     => 'other',
        self::PAYMENT_PROVIDER_ECOMMPAY  => 'ecommpay',
        self::PAYMENT_PROVIDER_WINPAY    => 'winpay',
        self::PAYMENT_PROVIDER_PAYANYWAY => 'payanyway',
        self::PAYMENT_PROVIDER_FATPAY    => 'fatpay',
        self::PAYMENT_PROVIDER_DUSUPAY   => 'dusupay',
        self::PAYMENT_PROVIDER_SPEEDPAY  => 'speedpay',
    ];
}
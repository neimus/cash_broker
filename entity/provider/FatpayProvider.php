<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 02.04.18
 */

namespace app\entity\provider;

class FatpayProvider extends AbstractProvider
{
    /**
     * @return int
     */
    public function getCodeProvider(): int
    {
        return self::PAYMENT_PROVIDER_FATPAY;
    }
}
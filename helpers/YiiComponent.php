<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 02.04.18
 */

namespace app\helpers;

use app\components\CurrencyConverter;
use app\components\FinanceManager;
use Yii;
use yii\mutex\MysqlMutex;
use yii\queue\amqp_interop\Queue;

class YiiComponent
{
    /**
     * @return MysqlMutex
     */
    public static function getMutex(): MysqlMutex
    {
        return Yii::$app->mutex;
    }

    /**
     * @return FinanceManager
     */
    public static function getFinanceManager(): FinanceManager
    {
        return Yii::$app->financeManager;
    }

    /**
     * @return CurrencyConverter
     */
    public static function getCurrencyConverter(): CurrencyConverter
    {
        return Yii::$app->currencyConverter;
    }

    /**
     * @return Queue
     */
    public static function getQueueJob(): Queue
    {
        return Yii::$app->queueJob;
    }

    /**
     * @return Queue
     */
    public static function getQueueEvent(): Queue
    {
        return Yii::$app->queueEvent;
    }
}
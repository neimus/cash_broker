<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\models;

use app\models\query\PayoutQuery;

/**
 * Модель для таблицы payout
 */
class Payout extends BasePayment
{
    // новая выплата, ещё не обрабатывалась
    const STATUS_NEW = 'new';
    // выплата была отправлена в биллинг, но не прошла валидацию
    const STATUS_MALFORMED = 'malformed';
    // выплата была отправлена в биллинг, но в биллинге уже есть выплата с таким ID
    const STATUS_DUPLICATE = 'duplicate';
    // выплата принята биллингом и ожидает одобрения оператором
    const STATUS_PENDING = 'pending';
    // оператор биллинга отклонил выплату в процессе рассмотрения или по результатам обработки платёжной системой
    const STATUS_REJECTED = 'rejected';
    // оператор биллинга одобрил выплату, и она ожидает отправки в платёжную систему
    const STATUS_APPROVED_BY_BILLING_OPERATOR = 'approved_by_billing_operator';
    // выплата передана в платёжную систему и находится в обработке
    const STATUS_IN_PROCESSING = 'in_processing';
    // временная ошибка платёжной системы - выплата всё ещё находится в обработке
    const STATUS_PAYMENT_GATEWAY_TEMPORARY_ERROR = 'payment_gateway_temporary_error';
    // выплата совершена
    const STATUS_COMPLETED = 'completed';
    // ошибка - подразумевает расследование инцидента и (возможно) ручное исправление ситуации
    const STATUS_ERROR = 'error';

    public static $rollbackStatuses = [
        self::STATUS_MALFORMED,
        self::STATUS_REJECTED,
    ];

    public static $terminalStatuses = [
        self::STATUS_MALFORMED,
        self::STATUS_REJECTED,
        self::STATUS_DUPLICATE,
        self::STATUS_COMPLETED,
        self::STATUS_ERROR,
    ];

    public static $nonTerminalStatuses = [
        self::STATUS_NEW,
        self::STATUS_PENDING,
        self::STATUS_APPROVED_BY_BILLING_OPERATOR,
        self::STATUS_IN_PROCESSING,
        self::STATUS_PAYMENT_GATEWAY_TEMPORARY_ERROR,
    ];

    public static $waitingStatuses = [
        self::STATUS_NEW,
        self::STATUS_PAYMENT_GATEWAY_TEMPORARY_ERROR,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'payout';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            self::COL_ID                  => 'ID',
            self::COL_USER_ID             => 'Пользователь',
            self::COL_STATUS              => 'Статус',
            self::COL_CURRENCY            => 'Валюта',
            self::COL_AMOUNT              => 'Сумма',
            self::COL_PAYMENT_PROVIDER    => 'Провайдер, через которого осуществляется платеж',
            self::COL_PAYMENT_METHOD      => 'Метод платежа',
            self::COL_EXTERNAL_PAYMENT_ID => 'Внешний ID платежа полученный от провайдера',
            self::COL_PAYMENT_DETAIL_HASH => 'Сведения о счета в виде hash-а',
            self::COL_RAW_DATA            => 'Дополнительные данные от провайдера',
            self::COL_CREATED_AT          => 'Дата создания',
            self::COL_UPDATED_AT          => 'Дата обновления информации',
        ];
    }

    public static function getAvailableStatuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_MALFORMED,
            self::STATUS_DUPLICATE,
            self::STATUS_PENDING,
            self::STATUS_REJECTED,
            self::STATUS_APPROVED_BY_BILLING_OPERATOR,
            self::STATUS_IN_PROCESSING,
            self::STATUS_PAYMENT_GATEWAY_TEMPORARY_ERROR,
            self::STATUS_COMPLETED,
            self::STATUS_ERROR,
        ];
    }

    /**
     * @inheritdoc
     * @return PayoutQuery активный запрос используемый AR классом.
     */
    public static function find(): PayoutQuery
    {
        return new PayoutQuery(static::class);
    }

    public function isInTerminalStatus(): bool
    {
        return \in_array($this->status, self::$terminalStatuses, true);
    }

    public function isInNonTerminalStatus(): bool
    {
        return \in_array($this->status, self::$nonTerminalStatuses, true);
    }

    public function allowRollback(): bool
    {
        return !$this->isInWaitingStatus() && !$this->isInTerminalStatus();
    }

    public function isInWaitingStatus(): bool
    {
        return \in_array($this->status, self::$waitingStatuses, true);
    }

    public function __toString()
    {
        try {
            return sprintf('Выплата со счета %d в сумме %s', $this->user_id, (string)$this->getAmountMoney());
        } catch (\InvalidArgumentException $exception) {
            return $exception->getMessage();
        }
    }

}
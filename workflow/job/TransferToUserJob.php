<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow\job;

use app\exception\UserNotFoundException;
use app\helpers\YiiComponent;
use app\models\User;
use app\workflow\AbstractFinanceJob;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class TransferToUserJob extends AbstractFinanceJob
{
    /**
     * @var int|string
     */
    public $fromUserId;

    /**
     * @var \app\models\User
     */
    private $fromUser;

    /**
     * @param array $data
     *
     * @throws \Exception
     */
    public function startJob(array &$data): void
    {
        YiiComponent::getFinanceManager()
            ->makeTransferToUser($this->getFromUser(), $this->getToUser(), $this->getAmount());
    }

    /**
     * @return array
     */
    protected function getContext(): array
    {
        return ArrayHelper::merge(['fromUserId' => $this->toUserId], parent::getContext());
    }

    /**
     * @return string
     */
    protected static function getEventName(): string
    {
        return 'transfer_to_user_event';
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validated(): void
    {
        if ($this->toUserId === null || $this->amount === null || $this->toUserId !== null) {
            throw new InvalidConfigException('Отсутствуют обязательные параметры');
        }
    }

    /**
     * @return User
     * @throws UserNotFoundException
     */
    protected function getFromUser(): User
    {
        if ($this->fromUser === null) {
            $this->fromUser = User::find()->getById($this->fromUserId);
            if ($this->fromUser === null) {
                throw new UserNotFoundException(sprintf('Пользователь с ID %s не найден', $this->toUserId));
            }
        }

        return $this->fromUser;
    }
}
<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id'             => 'user-balance',
    'name'           => 'Микросервис баланса пользователей',
    'language'       => 'ru',
    'sourceLanguage' => 'ru-RU',
    'basePath'       => dirname(__DIR__),
    'bootstrap'      => ['log'],
    'aliases'        => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'     => [
        'formatter'         => [
            'timeZone' => 'Europe/Moscow',
        ],
        'request'           => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Zh8vl5BbqXSmHI5MUW088RKIil4YzfcL',
        ],
        'cache'             => [
            'class' => 'yii\caching\FileCache',
        ],
        // 'user' => [
        //     'identityClass' => 'app\models\User',
        //     'enableAutoLogin' => false,
        // ],
        'errorHandler'      => [
            'errorAction' => 'site/error',
        ],
        'log'               => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'                => $db,
        'mutex'             => [
            'class' => 'yii\mutex\MysqlMutex',
        ],
        'financeManager'    => [
            'class' => 'app\components\FinanceManager',
        ],
        'currencyConverter' => [
            'class'                     => 'app\components\CurrencyConverter',
            'classCurrencyRateProvider' => 'app\components\rates\DbCurrencyRatesProvider',
        ],
        'queueJob'          => [
            'class'         => \yii\queue\amqp_interop\Queue::class,
            'ttr'           => 5 * 60, // Макимальное время жизни
            'attempts'      => 5, // Максимальное кол-во попыток при ошибках
            'exchangeName'  => 'exchange_job',
            'queueName'     => 'queue_job',
            'driver'        => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'strictJobType' => false,
            'serializer'    => app\workflow\JobSerializer::class,
        ],
        'queueEvent'        => [
            'class'         => \yii\queue\amqp_interop\Queue::class,
            'ttr'           => 5 * 60, // Макимальное время жизни
            'attempts'      => 5, // Максимальное кол-во попыток при ошибках
            'exchangeName'  => 'exchange_event',
            'queueName'     => 'queue_event',
            'driver'        => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'strictJobType' => false,
            'serializer'    => app\workflow\EventSerializer::class,
        ],
    ],
    'params'         => $params,
];

require __DIR__ . '/queue.php';

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;

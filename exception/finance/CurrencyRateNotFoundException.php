<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\exception\finance;

class CurrencyRateNotFoundException extends \RuntimeException
{
    public static function forCurrencies(string $from, string $to)
    {
        return new self(sprintf('Коэффициент валюты %s/%s не найден', $from, $to));
    }
}

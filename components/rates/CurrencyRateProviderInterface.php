<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\components\rates;

use app\entity\Currency;

interface CurrencyRateProviderInterface
{
    public function getCurrencyRatesSnapshot(): array;

    public function getCurrencyRate(Currency $currencyFrom, Currency $currencyTo);
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow;

use app\entity\Money;
use app\entity\provider\AbstractProvider;
use app\exception\finance\InvalidPaymentCodeException;
use app\exception\MutexAcquireException;
use app\exception\UserNotFoundException;
use app\helpers\YiiComponent;
use app\models\User;
use yii\base\InvalidConfigException;

abstract class AbstractFinanceJob extends AbstractJob
{
    /**
     * Максимальное время блокировки (секунды)
     */
    const MUTEX_LOCK_TIMEOUT = 20;

    /**
     * @var int|string
     */
    public $toUserId;

    /**
     * @var string вида 'RUB 1354.25'
     */
    public $amount;

    /**
     * @var int|string
     */
    public $paymentCode;

    /**
     * @var string
     */
    public $rawData = '';

    /**
     * @var string
     */
    public $externalPaymentId;

    /**
     * @var string
     */
    public $status;

    /**
     * @var User
     */
    private $toUser;

    /**
     * @var bool
     */
    private $isLocked = false;

    /**
     * @var
     */
    private $lockName;

    /**
     * @param \yii\queue\Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        $data = [];
        try {
            static::validated();
            $this->startJob($data);
            $this->sendSuccessEvent($data);

        } catch (MutexAcquireException $exception) {
            $this->logErrorMessage($exception);
            //    TODO: Добавить повторный запуск для задачи
            $this->sendErrorEvent($exception->getMessage());

        } catch (\Exception $exception) {
            $this->logErrorMessage($exception);
            $this->sendErrorEvent($exception->getMessage());
        } finally {
            $this->mutexRelease();
        }
    }

    /**
     * @param array $data
     */
    abstract public function startJob(array &$data): void;

    /**
     * @throws InvalidConfigException
     */
    abstract protected function validated(): void;

    /**
     * @return User
     * @throws UserNotFoundException
     */
    protected function getToUser(): User
    {
        if ($this->toUser === null) {
            $this->toUser = User::find()->getById($this->toUserId);
            if ($this->toUser === null) {
                throw new UserNotFoundException(sprintf('Пользователь с ID %s не найден', $this->toUserId));
            }
        }

        return $this->toUser;
    }

    /**
     * @return Money
     * @throws \InvalidArgumentException
     */
    protected function getAmount(): Money
    {
        return Money::createFromString($this->amount);
    }

    /**
     * @return AbstractProvider
     *
     * @throws InvalidPaymentCodeException
     * @throws InvalidConfigException
     */
    protected function getProvider(): AbstractProvider
    {
        $provider = PaymentTypesMap::getProviderByPaymentCode((int)$this->paymentCode);
        $method = PaymentTypesMap::getMethod((int)$this->paymentCode);
        if ($method === null || $provider === null) {
            throw new InvalidPaymentCodeException(sprintf('Код платежа %s не найден', $this->paymentCode));
        }

        $provider
            ->setMethod($method)
            ->setRawData($this->rawData)
            ->setExternalId($this->externalPaymentId);

        return $provider;
    }

    protected function getContext(): array
    {
        return [
            'toUserId'          => $this->toUserId,
            'amount'            => $this->amount,
            'paymentCode'      => $this->paymentCode,
            'rawData'           => $this->rawData,
            'externalPaymentId' => $this->externalPaymentId,
            'status'            => $this->status,
        ];
    }

    /**
     * Попытка блокировки
     *
     * @param string $name
     *
     * @throws MutexAcquireException
     */
    protected function mutexAcquire(string $name): void
    {
        $this->lockName = $name;

        $this->isLocked = YiiComponent::getMutex()->acquire($this->lockName, self::MUTEX_LOCK_TIMEOUT);

        if ($this->isLocked === false) {
            throw new MutexAcquireException('Не удалось получить блокировку');
        }
    }

    /**
     * Разблокировка
     */
    protected function mutexRelease(): void
    {
        if ($this->isLocked && $this->lockName !== null) {
            YiiComponent::getMutex()->release($this->lockName);
        }
    }
}
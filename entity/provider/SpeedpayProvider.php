<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 02.04.18
 */

namespace app\entity\provider;

class SpeedpayProvider extends AbstractProvider
{
    /**
     * @return int
     */
    public function getCodeProvider(): int
    {
        return self::PAYMENT_PROVIDER_SPEEDPAY;
    }
}
<?php

use app\migrations\MigrationMySQL;

/**
 * Class m180401_104202_init
 */
class m180401_104202_init extends MigrationMySQL
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if ($this->dbType === 'mysql') {
            $this->beginUp();
            /**
             * **********************************
             *          ТАБЛИЦА USER
             * **********************************
             */
            $this->dropTableIsExists('user');
            $this->beginCreateTable();
            $this->createTable('user', [
                'id'         => $this->primaryKey(11)->unsigned()->comment('Id'),
                'username'   => $this->string(55)->notNull()->unique()->comment('Имя пользователя'),
                'currency'   => $this->smallInteger(6)->notNull()->comment('Валюта пользователя'),
                'created_at' => $this->dateTime()->notNull()->comment('Дата создания'),
                'is_deleted' => $this->boolean()->notNull()->defaultValue(false)
                    ->comment('Флаг для удаления пользователя'),
            ], $this->getOptions());
            $this->addCommentOnTable('user', 'Пользователи');

            $this->endCreateTable();

            /**
             * **********************************
             *          ТАБЛИЦА REFILL
             * **********************************
             */
            $this->dropTableIsExists('refill');
            $this->beginCreateTable();
            $this->createTable('refill', [
                'id'                  => $this->primaryKey(11)->unsigned()->comment('Id'),
                'user_id'             => $this->integer(11)->notNull()->unsigned()
                    ->comment('Id пользователя, для которого производится операция'),
                'status'              => "ENUM('new', 'waiting', 'approved', 'declined') NOT NULL DEFAULT 'new' COMMENT 'Статус'",
                'currency'            => $this->smallInteger(6)->notNull()->comment('Валюта'),
                'amount'              => $this->decimal(13, 4)->notNull()->comment('Сумма'),
                'payment_provider'    => $this->smallInteger(6)->notNull()
                    ->comment('Провайдер, через которого осуществляется платеж'),
                //TODO: Вынести в отдельную таблицу или использовать численное представление
                'payment_method'      => "ENUM('other', 'credit_card', 'yandex_money', 'sberbank_online', 'mobile_mts', 'mobile_beeline', 'mobile_tele2', 'mobile_megafon', 'qiwi_wallet', 'moneta_ru', 'alfaclick', 'bank_transfer', 'terminal_euroset', 'terminal_svyaznoy', 'webmoney') NOT NULL COMMENT 'Метод платежа'",
                'external_payment_id' => $this->string(255)->notNull()
                    ->comment('Внешний ID платежа полученный от провайдера'),
                'payment_detail_hash' => $this->string(32)->comment('Сведения о счета в виде hash-а'),
                'raw_data'            => $this->text()->comment('Дополнительные данные от провайдера'),
                'created_at'          => $this->dateTime()->notNull()->comment('Дата создания'),
                'updated_at'          => $this->dateTime()->notNull()->comment('Дата обновления информации'),

            ], $this->getOptions());
            $this->addCommentOnTable('refill', 'Пополнение счета');
            $this->endCreateTable();

            $this->setIndex('refill', 'user_id');
            $this->setIndex('refill', ['payment_provider', 'external_payment_id'], true);
            $this->setForeignKey('refill', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');

            /**
             * **********************************
             *          ТАБЛИЦА PAYOUT
             * **********************************
             */
            $this->dropTableIsExists('payout');
            $this->beginCreateTable();
            $this->createTable('payout', [
                'id'                  => $this->primaryKey(11)->unsigned()->comment('Id'),
                'user_id'             => $this->integer(11)->notNull()->unsigned()
                    ->comment('Id пользователя, для которого производится операция'),
                'status'              => "ENUM('new', 'malformed', 'duplicate', 'pending', 'rejected', 'approved_by_billing_operator', 'in_processing', 'payment_gateway_temporary_error', 'completed') NOT NULL DEFAULT 'new' COMMENT 'Статус'",
                'currency'            => $this->smallInteger(6)->notNull()->comment('Валюта'),
                'amount'              => $this->decimal(13, 4)->notNull()->comment('Сумма'),
                'payment_provider'    => $this->smallInteger(6)->notNull()
                    ->comment('Провайдер, через которого осуществляется платеж'),
                //TODO: Вынести в отдельную таблицу или использовать численное представление
                'payment_method'      => "ENUM('other', 'credit_card', 'yandex_money', 'sberbank_online', 'mobile_mts', 'mobile_beeline', 'mobile_tele2', 'mobile_megafon', 'qiwi_wallet', 'moneta_ru', 'alfaclick', 'bank_transfer', 'terminal_euroset', 'terminal_svyaznoy', 'webmoney') NOT NULL COMMENT 'Метод платежа'",
                'external_payment_id' => $this->string(255)->notNull()
                    ->comment('Внешний ID платежа полученный от провайдера'),
                'payment_detail_hash' => $this->string(32)->comment('Сведения о счета в виде hash-а'),
                'raw_data'            => $this->text()->comment('Дополнительные данные от провайдера'),
                'created_at'          => $this->dateTime()->notNull()->comment('Дата создания'),
                'updated_at'          => $this->dateTime()->notNull()->comment('Дата обновления информации'),

            ], $this->getOptions());
            $this->addCommentOnTable('payout', 'Списания со счета');
            $this->endCreateTable();

            $this->setIndex('payout', 'user_id');
            $this->setIndex('payout', ['payment_provider', 'external_payment_id'], true);
            $this->setForeignKey('payout', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');

            /**
             * **********************************
             *          ТАБЛИЦА TRANSACTION
             * **********************************
             */
            $this->dropTableIsExists('transaction');
            $this->beginCreateTable();
            $this->createTable('transaction', [
                'id'           => $this->primaryKey(11)->unsigned()->comment('Id'),
                'user_id'      => $this->integer(11)->notNull()->unsigned()
                    ->comment('Id пользователя, для которого производится операция'),
                'initiator_id' => $this->integer(11)->unsigned()
                    ->comment('Инициатор (пользователь) при проведении коррекции или списания'),
                'type'         => "ENUM('refill', 'refill_rollback', 'payout', 'payout_rollback', 'correction', 'write_off', 'bonus_activation', 'bonus_revocation', 'transfer_user', 'lockout', 'revoke_lockout') NOT NULL COMMENT 'Тип транзакции'",
                'refill_id'    => $this->integer(11)->unsigned()->comment('ID операции пополнения'),
                'payout_id'    => $this->integer(11)->unsigned()->comment('ID операции списания'),
                'account'      => $this->smallInteger(6)->notNull()->comment('Счет'),
                'currency'     => $this->smallInteger(6)->notNull()->comment('Валюта'),
                'amount'       => $this->decimal(13, 4)->notNull()->comment('Сумма'),
                'comment'      => $this->string(255)->comment('Комментарий'),
                'created_at'   => $this->dateTime()->notNull()->comment('Дата создания'),

            ], $this->getOptions());
            $this->addCommentOnTable('transaction', 'Транзакции');
            $this->endCreateTable();

            $this->setIndex('transaction', 'user_id');
            $this->setIndex('transaction', 'initiator_id');

            $this->setForeignKey('transaction', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');
            $this->setForeignKey('transaction', 'initiator_id', 'user', 'id', 'RESTRICT', 'CASCADE');

            $this->setForeignKey('transaction', 'refill_id', 'refill', 'id', 'RESTRICT', 'CASCADE');
            $this->setForeignKey('transaction', 'payout_id', 'payout', 'id', 'RESTRICT', 'CASCADE');

            /**
             * **********************************
             *          ТАБЛИЦА CURRENCY_RATE
             * **********************************
             */
            $this->dropTableIsExists('currency_rate');
            $this->beginCreateTable();
            $this->createTable('currency_rate', [
                'id'            => $this->primaryKey(11)->unsigned()->comment('Id'),
                'currency_from' => $this->char(3)->notNull()->comment('Валюта из которого осуществляется конвертация'),
                'currency_to'   => $this->char(3)->notNull()->comment('Валюта в которую осуществляется конвертация'),
                'ratio'         => $this->decimal(11, 5)->notNull()->comment('Коэффициент'),

            ], $this->getOptions());
            $this->addCommentOnTable('currency_rate', 'Коэффициенты валют');
            $this->endCreateTable();

            $this->setIndex('currency_rate', ['currency_from', 'currency_to'], true);

            $this->endUp();
        } else {
            echo 'The database is not MySQL format, the migration is not possible';
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if ($this->dbType === 'mysql') {
            $this->beginDown();
            $this->dropTableIsExists('transaction');
            $this->dropTableIsExists('refill');
            $this->dropTableIsExists('payout');
            $this->dropTableIsExists('user');
            $this->dropTableIsExists('currency_rate');
            $this->endDown();

            return true;
        }

        echo 'The database is not MySQL format, the migration is not possible';

        return false;
    }
}

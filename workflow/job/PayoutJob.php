<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow\job;

use app\exception\finance\InvalidStatusException;
use app\helpers\YiiComponent;
use app\models\Payout;
use app\workflow\AbstractFinanceJob;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class PayoutJob extends AbstractFinanceJob
{
    /**
     * @var string|int
     */
    public $payoutId;

    /**
     * @var Payout
     */
    private $payout;

    /**
     * @param array $data
     *
     * @throws \RuntimeException
     * @throws \app\exception\MutexAcquireException
     * @throws \app\exception\finance\InvalidStatusException
     * @throws \app\exception\finance\TransactionException
     * @throws \app\exception\finance\FinanceTransferInvalidArgument
     * @throws \app\exception\finance\CurrencyRateNotFoundException
     * @throws \app\exception\finance\InvalidPaymentCodeException
     * @throws \app\exception\UserNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws \app\exception\finance\FinanceManagerException
     * @throws \yii\base\InvalidConfigException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function startJob(array &$data): void
    {
        if ($this->status === Payout::STATUS_NEW) {
            $this->payout = YiiComponent::getFinanceManager()
                ->createPayout($this->getToUser(), $this->getAmount(), $this->getProvider());
            $data['payoutId'] = $this->payout->id;
        } else {
            $this->mutexAcquire('payout_id_' . $this->getPayout()->id);
            switch ($this->status) {
                case Payout::STATUS_PAYMENT_GATEWAY_TEMPORARY_ERROR:
                    break;
                case Payout::STATUS_APPROVED_BY_BILLING_OPERATOR:
                case Payout::STATUS_PENDING:
                case Payout::STATUS_IN_PROCESSING:
                    if ($this->getPayout()->isInWaitingStatus()) {
                        YiiComponent::getFinanceManager()->makePayout($this->getPayout());
                    }
                    break;
                case Payout::STATUS_COMPLETED:
                    if (!$this->getPayout()->isInNonTerminalStatus()) {
                        if ($this->getPayout()->isInWaitingStatus()) {
                            YiiComponent::getFinanceManager()->makePayout($this->getPayout());
                        }
                        YiiComponent::getFinanceManager()->confirmPayout($this->getPayout());
                    }
                    break;
                case Payout::STATUS_MALFORMED:
                case Payout::STATUS_DUPLICATE:
                case Payout::STATUS_REJECTED:
                    if (!$this->getPayout()->allowRollback()) {
                        YiiComponent::getFinanceManager()->rollbackPayout($this->getPayout());
                    }
                    break;
                case Payout::STATUS_ERROR:
                    if (!$this->getPayout()->allowRollback()) {
                        YiiComponent::getFinanceManager()->rollbackPayout($this->getPayout());
                    }
                    if ($this->getPayout() !== Payout::STATUS_ERROR) {
                        YiiComponent::getFinanceManager()
                            ->makeLockout($this->getPayout()->getUser(), $this->getPayout()->getAmountMoney());
                    }
                    break;

                default:
                    throw new InvalidStatusException(sprintf('Некорректный статус %s', $this->status));
            }

            $this->getPayout()->setStatus($this->status)->save();
        }
    }

    /**
     * @throws \app\exception\finance\InvalidStatusException
     * @throws InvalidConfigException
     */
    protected function validated(): void
    {
        if ($this->status === null) {
            throw new InvalidStatusException('Отсутствуют обязательный параметр [status]');
        }

        if ($this->status === Payout::STATUS_NEW &&
            ($this->toUserId === null || $this->amount === null
             || $this->paymentCode === null || $this->externalPaymentId === null)) {
            throw new InvalidConfigException('Отсутствуют обязательные параметры');
        }
    }

    protected function getContext(): array
    {
        return ArrayHelper::merge(['payoutId' => $this->payoutId], parent::getContext());
    }

    /**
     *
     * @throws \InvalidArgumentException
     */
    private function getPayout(): Payout
    {
        if ($this->payout === null) {
            if ($this->payoutId === null) {
                throw new \InvalidArgumentException(sprintf('Не передан ID для пополнения'));
            }
            $this->payout = Payout::findOne(['=', Payout::COL_ID, $this->payoutId]);
        }

        return $this->payout;
    }

    protected static function getEventName(): string
    {
        return 'payout_event';
    }
}
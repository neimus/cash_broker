<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.2018 */

namespace app\models\query;

use app\models\User;
use yii\db\ActiveQuery;

/**
 * ActiveQuery для [[User]].
 * @see User
 */
class UserQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return User[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return User|null
     */
    public function one($db = null): ?User
    {
        return parent::one($db);
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function getByUsername(string $username): ?User
    {
        return $this->where(['=', User::COL_USERNAME, $username])
            ->andWhere(['=', User::COL_IS_DELETED, User::NOT_DELETED])
            ->one();
    }

    /**
     * @param int|string $id
     *
     * @return User|null
     */
    public function getById($id): ?User
    {
        return $this->where(['=', User::COL_ID, $id])
            ->andWhere(['=', User::COL_IS_DELETED, User::NOT_DELETED])
            ->one();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 03.04.18
 */

namespace app\workflow\job;

use app\exception\finance\InvalidStatusException;
use app\helpers\YiiComponent;
use app\models\Refill;
use app\workflow\AbstractFinanceJob;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class RefillJob extends AbstractFinanceJob
{
    /**
     * @var string|int
     */
    public $refillId;

    /**
     * @var Refill
     */
    private $refill;

    /**
     * @param array $data
     *
     * @throws \app\exception\MutexAcquireException
     * @throws \app\exception\finance\InvalidStatusException
     * @throws \app\exception\finance\TransactionException
     * @throws \app\exception\finance\FinanceTransferInvalidArgument
     * @throws \app\exception\finance\CurrencyRateNotFoundException
     * @throws \app\exception\finance\InvalidPaymentCodeException
     * @throws \app\exception\UserNotFoundException
     * @throws \yii\base\InvalidArgumentException
     * @throws \app\exception\finance\FinanceManagerException
     * @throws \yii\base\InvalidConfigException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function startJob(array &$data): void
    {
        if ($this->status === Refill::STATUS_NEW) {
            $this->refill = YiiComponent::getFinanceManager()
                ->createRefill($this->getToUser(), $this->getAmount(), $this->getProvider());
            $data['refillId'] = $this->refill->id;
        } else {
            $this->mutexAcquire('refill_id_' . $this->getRefill()->id);
            switch ($this->status) {
                case Refill::STATUS_WAITING:
                    break;
                case Refill::STATUS_APPROVED:
                    if (!$this->getRefill()->isInTerminalStatus()) {
                        YiiComponent::getFinanceManager()->makeRefill($this->getRefill());
                    }
                    break;
                case Refill::STATUS_DECLINED:
                    if ($this->getRefill()->allowRollback()) {
                        YiiComponent::getFinanceManager()->rollbackRefill($this->getRefill());
                    }
                    break;
                default:
                    throw new InvalidStatusException(sprintf('Некорректный статус %s', $this->status));
            }

            $this->getRefill()->setStatus($this->status)->save();
        }
    }

    protected function getContext(): array
    {
        return ArrayHelper::merge(['refillId' => $this->refillId], parent::getContext());
    }

    /**
     *
     * @throws \InvalidArgumentException
     */
    private function getRefill(): Refill
    {
        if ($this->refill === null) {
            if ($this->refillId === null) {
                throw new \InvalidArgumentException(sprintf('Не передан ID для пополнения'));
            }
            $this->refill = Refill::findOne(['=', Refill::COL_ID, $this->refillId]);
        }

        return $this->refill;
    }

    protected static function getEventName(): string
    {
        return 'refill_event';
    }

    /**
     * @throws \app\exception\finance\InvalidStatusException
     * @throws InvalidConfigException
     */
    protected function validated(): void
    {
        if ($this->status === null) {
            throw new InvalidStatusException('Отсутствуют обязательный параметр [status]');
        }

        if ($this->status === Refill::STATUS_NEW &&
            ($this->toUserId === null || $this->amount === null
             || $this->paymentCode === null || $this->externalPaymentId === null)) {
            throw new InvalidConfigException('Отсутствуют обязательные параметры');
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 01.04.18
 */

namespace app\exception\finance;

use app\entity\FinanceTransfer;

class FinanceTransferInvalidArgument extends \InvalidArgumentException
{
    public static function createFromFinanceTransfer(FinanceTransfer $financeTransfer)
    {
        return new self(sprintf('Некорректный финансовый перевод %s', $financeTransfer));
    }
}
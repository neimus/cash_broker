<?php
/**
 * Created by PhpStorm.
 * User: Saburov Denis
 * Date: 31.03.18
 */

namespace app\models;

use app\models\query\CurrencyRateQuery;

/**
 * @property int $id
 * @property string $currency_from
 * @property string $currency_to
 * @property double $ratio
 */
class CurrencyRate extends AbstractModel
{
    const COL_ID            = 'id';
    const COL_CURRENCY_FROM = 'currency_from';
    const COL_CURRENCY_TO   = 'currency_to';
    const COL_RATIO         = 'ratio';

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'currency_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                self::COL_RATIO,
                'double',
                'min' => 0,
                'max' => 999999.99999,
            ],
            [
                [
                    self::COL_CURRENCY_FROM,
                    self::COL_CURRENCY_TO,
                ],
                'string',
                'length' => 3,
            ],
            [
                [
                    self::COL_CURRENCY_FROM,
                    self::COL_CURRENCY_TO,
                    self::COL_RATIO,
                ],
                'required',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            self::COL_ID            => 'ID',
            self::COL_CURRENCY_FROM => 'Валюта из которого осуществляется конвертация',
            self::COL_CURRENCY_TO   => 'Валюта в которую осуществляется конвертация',
            self::COL_RATIO         => 'Коэффициент отношения двух валют',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints(): array
    {
        return $this->attributeLabels();
    }

    /**
     * @inheritdoc
     * @return CurrencyRateQuery активный запрос используемый AR классом.
     */
    public static function find(): CurrencyRateQuery
    {
        return new CurrencyRateQuery(static::class);
    }

    public function __toString()
    {
        return sprintf('%s -> %s (%d)', $this->currency_from, $this->currency_to, $this->ratio);
    }

}